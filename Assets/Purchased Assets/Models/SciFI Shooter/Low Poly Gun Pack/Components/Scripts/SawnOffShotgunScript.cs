﻿using UnityEngine;
using System.Collections;

public class SawnOffShotgunScript : MonoBehaviour {

	bool outOfAmmo = false;
	bool isReloading = false;

	[Header("Spawnpoints & Prefabs")]
	public Transform casingSpawnPoint1;
	public Transform casingSpawnPoint2;
	public Transform casingPrefab;
	[Header("Components")]
	public GameObject holder;
	public GameObject barrels;
	public GameObject shotgunAmmo1;
	public GameObject shotgunAmmo2;
	public GameObject ammo1;
	public GameObject ammo2;
	public ParticleSystem smokeParticles;
	public GameObject sideMuzzle;
	public GameObject topMuzzle;
	int magazineSize = 2;
	int bulletsLeft;
	float reloadDuration = 1.5f;
	[Header("Customizable Options")]
	public float muzzleFlashDuration;
	public Light lightFlash;
	[Range(1f, 4f)]
	public float lightIntensity;
	[Range(5f, 50f)]
	public float lightRange;
	
	void Start () {
		//Set the magazine size
		bulletsLeft = magazineSize;
		
		//Make sure the light is off
		lightFlash.GetComponent<Light>().enabled = false;
		
		//Set the light values
		lightFlash.intensity = lightIntensity;
		lightFlash.range = lightRange;
		
		//Hide the muzzleflashes at start
		sideMuzzle.GetComponent<Renderer>().enabled = false;
		topMuzzle.GetComponent<Renderer>().enabled = false;
	}
	
	IEnumerator Reload () {
		//Start reloading
		isReloading = true;
		
		//Hide the shellcasings
		shotgunAmmo1.GetComponent<MeshRenderer>().enabled = false;
		shotgunAmmo2.GetComponent<MeshRenderer>().enabled = false;
		
		//Play reload and mag in animation
		holder.GetComponent<Animation> ().Play ("sawn_off_shotgun_reload_down");
		barrels.GetComponent<Animation> ().Play ("sawn_off_shotgun_barrels_open");

		//Wait
		yield return new WaitForSeconds (0.15f);
		
		//Spawn casings
		Instantiate(casingPrefab, casingSpawnPoint1.transform.position, casingSpawnPoint1.transform.rotation);
		Instantiate(casingPrefab, casingSpawnPoint2.transform.position, casingSpawnPoint2.transform.rotation);

		//Animate casings in
		ammo1.GetComponent<Animation> ().Play ("sawn_off_shotgun_ammo_one_in");
		ammo2.GetComponent<Animation> ().Play ("sawn_off_shotgun_ammo_two_in");

		//Wait for set amount of time
		yield return new WaitForSeconds(reloadDuration);

		//Play animations
		holder.GetComponent<Animation> ().Play ("sawn_off_shotgun_reload_up");
		barrels.GetComponent<Animation> ().Play ("sawn_off_shotgun_barrels_close");
		
		//Refill bullets
		bulletsLeft = magazineSize;
		
		//Make the shellcasings visible again
		shotgunAmmo1.GetComponent<MeshRenderer>().enabled = true;
		shotgunAmmo2.GetComponent<MeshRenderer>().enabled = true;

		//Wait until animations are finished
		yield return new WaitForSeconds (0.1f);

		//Enable shooting again
		outOfAmmo = false;
		isReloading = false;
	}	
	
	IEnumerator Muzzleflash () {
		//Show the muzzleflashes
		sideMuzzle.GetComponent<SpriteRenderer>().enabled = true;
		topMuzzle.GetComponent<SpriteRenderer>().enabled = true;

		//Show the light
		lightFlash.GetComponent<Light>().enabled = true;
		
		//Wait for set amount of time
		yield return new WaitForSeconds(muzzleFlashDuration);
		
		//Hide the muzzleflashes
		sideMuzzle.GetComponent<SpriteRenderer>().enabled = false;
		topMuzzle.GetComponent<SpriteRenderer>().enabled = false;
		
		//Hide the light
		lightFlash.GetComponent<Light>().enabled = false;
	}	
	
	void Update () {
		//Shoot when left click is pressed
		if (Input.GetMouseButtonDown(0) && !outOfAmmo && !isReloading) {

				//Muzzleflash
				StartCoroutine(Muzzleflash());
				
				//Play recoil and eject animations
				holder.GetComponent<Animation>().Play("sawn_off_shotgun_recoil");
				//ejectSlider.GetComponent<Animation>().Play("eject_slider_anim");
				
				//Remove 1 bullet everytime you shoot
				bulletsLeft -=1;
				
				//Play smoke particles
				smokeParticles.Play();
		}

		//Reload when R key is pressed
		if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !isReloading) {
			StartCoroutine(Reload());
		}
		
		//If out of ammo
		if (bulletsLeft == 0) {
			outOfAmmo = true;
		}
	}
}