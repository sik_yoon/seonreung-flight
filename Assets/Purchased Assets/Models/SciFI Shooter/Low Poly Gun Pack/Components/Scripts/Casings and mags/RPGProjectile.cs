﻿using UnityEngine;
using System.Collections;

public class RPGProjectile :MonoBehaviour {

	public Transform explosionPrefab;
	[Header("Customizable Options")]
	public float speed;
	public float despawnTime;
	public float radius = 5.0F;
	public float power = 10.0F;

	void Start () {
		//Start the remove coroutine
		StartCoroutine (DestroyTimer ());
	}

	void Update () {
		//Launch the projectile forward
		transform.Translate(Vector3.up * Time.deltaTime * speed);
	}

	IEnumerator DestroyTimer () {
		//Destroy the projectile after set amount of seconds
		yield return new WaitForSeconds (despawnTime);
		Destroy (gameObject);
	}

	//If the projectile collides with anything
	void OnCollisionEnter (Collision collision) {
		//Spawn explosion prefab
		Instantiate(explosionPrefab, gameObject.transform.position, gameObject.transform.rotation);

		//Destroy projectile
		Destroy (gameObject);

		//Explosion force
		Vector3 explosionPos = transform.position;
		Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
		foreach (Collider hit in colliders) {
			Rigidbody rb = hit.GetComponent<Rigidbody> ();
			
			if (rb != null)
				rb.AddExplosionForce (power, explosionPos, radius, 3.0F);
		}}
	}