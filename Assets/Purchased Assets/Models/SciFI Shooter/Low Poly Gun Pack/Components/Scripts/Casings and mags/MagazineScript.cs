﻿using UnityEngine;
using System.Collections;

public class MagazineScript : MonoBehaviour {

	[Header("Customizable Options")]
	public float despawnTimer;
	public float ejectForce;
	
	void Awake () {
		//Eject magazine downwards with set amount of force
		//Useful so the mag doesnt get stuck inside the gun
		GetComponent<Rigidbody>().AddRelativeForce (0,0,ejectForce); 
	}

	void Start () {
		//Start the remove coroutine
		StartCoroutine(DespawnTimer());
	}

	IEnumerator DespawnTimer () {
		//Destroy the magazine after set amount of time	
		yield return new WaitForSeconds(despawnTimer);
		Destroy (gameObject);
	}
}