﻿using UnityEngine;
using System.Collections;

public class RPGExplosionScript : MonoBehaviour {

	[Header("Customizable Options")]
	public float despawnTime = 2.5f;
	public float lightDuration = 0.02f;
	public Light lightFlash;

	void Start () {
		//Start the coroutines
		StartCoroutine (DestroyTimer ());
		StartCoroutine (LightFlash ());
	}

	IEnumerator LightFlash () {
		//Show the light
		lightFlash.GetComponent<Light>().enabled = true;
		yield return new WaitForSeconds (lightDuration);
		//Hide the light
		lightFlash.GetComponent<Light>().enabled = false;
	}

	IEnumerator DestroyTimer () {
		//Destroy the explosion prefab after set amount of seconds
		yield return new WaitForSeconds (despawnTime);
		Destroy (gameObject);
	}
}
