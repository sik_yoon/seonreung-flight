﻿using UnityEngine;
using System.Collections;

public class MachineGunScript : MonoBehaviour {

	bool outOfAmmo = false;
	bool isReloading = false;
	//Used for firerate
	float lastFired;

	[Header("Spawnpoints & Prefabs")]
	public Transform casingSpawnPoint;
	public Transform casingPrefab;
	public Transform magSpawnPoint;
	public Transform emptyMagPrefab;
	[Header("Components")]
	public GameObject holder;
	public GameObject slider;
	public GameObject mag;
	public GameObject fullMag;
	public GameObject top;
	public GameObject bulletHolderAnim;
	public ParticleSystem smokeParticles;
	public GameObject sideMuzzle;
	public GameObject topMuzzle;
	public GameObject frontMuzzle;
	public Sprite[] muzzleflashSideSprites;
	public GameObject[] bullets;
	public Transform[] bulletSpawnpoints;
	public Transform bulletPrefab;
	public GameObject sphereRotate;
	[Header("Customizable Options")]
	public int magazineSize;
	int bulletsLeft;
	float reloadDuration = 2f;
	public float muzzleFlashDuration;
	public float fireRate;
	public Light lightFlash;
	[Range(1f, 4f)]
	public float lightIntensity;
	[Range(5f, 50f)]
	public float lightRange;


	void Start () {
		//Make sure the bullets in the bullet belt are "frozen" properly
		foreach (GameObject bulletObjects in bullets)
		{
			bulletObjects.GetComponent<Rigidbody>().constraints = 
				//Rotation
				RigidbodyConstraints.FreezeRotationX | 
				RigidbodyConstraints.FreezeRotationY | 
				RigidbodyConstraints.FreezeRotationZ | 
				//Position
				RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;
		}

		bulletHolderAnim.SetActive (false);

		//Set the magazine size
		bulletsLeft = magazineSize;
		
		//Make sure the light is off
		lightFlash.GetComponent<Light>().enabled = false;
		
		//Set the light values
		lightFlash.intensity = lightIntensity;
		lightFlash.range = lightRange;
		
		//Hide the muzzleflashes at start
		sideMuzzle.GetComponent<Renderer>().enabled = false;
		topMuzzle.GetComponent<Renderer>().enabled = false;
		frontMuzzle.GetComponent<SpriteRenderer>().enabled = false;
	}

	//If reloading when bullets left = 0
	IEnumerator Reload () {
		//Start reloading
		isReloading = true;

		//Hide all bullets when reloading
		foreach (GameObject bulletObjects in bullets)
		{
			bulletObjects.gameObject.GetComponent<MeshRenderer>().enabled = false;
		}

		//Hide the magazine
		mag.SetActive (false);

		//Instantiate the empty magazine 
		Instantiate(emptyMagPrefab, magSpawnPoint.transform.position, magSpawnPoint.transform.rotation);
		
		//Play reload animation
		holder.GetComponent<Animation>().Play("machine_gun_reload");
		//Wait for animation to finish
		yield return new WaitForSeconds (0.25f);
		//Open the top and play the mag animation
		top.GetComponent<Animation> ().Play ("machine_gun_top_open");
		fullMag.GetComponent<Animation>().Play("machine_gun_mag_in");
		//Wait for set amount of time
		yield return new WaitForSeconds(reloadDuration);

		//Refill bullets
		bulletsLeft = magazineSize;
		
		//Make the magazine visible again and play animation
		mag.SetActive (true);
		bulletHolderAnim.SetActive (true);
		bulletHolderAnim.GetComponent<Animation> ().Play ("machine_gun_bullet_holder");

		//Wait for ammo animation to finish
		yield return new WaitForSeconds (1f);
		//Close the top lid
		top.GetComponent<Animation> ().Play ("machine_gun_top_close");
		//Wait for the top lid animation to finish
		yield return new WaitForSeconds (0.15f);
		//Slider animation
		slider.GetComponent<Animation>().Play("machine_gun_slider_eject");

		//Hide the animated bullets
		bulletHolderAnim.SetActive (false);

		//Enable shooting again
		outOfAmmo = false;
		isReloading = false;

		//Show the bullet belt again
		foreach (GameObject bulletObjects in bullets)
		{
			bulletObjects.gameObject.GetComponent<MeshRenderer>().enabled = true;
		}
	}	

	//If reloading when bullets left is higher than 0
	IEnumerator Reload2 () {
		//Start reloading
		isReloading = true;

		//Hide all bullets when reloading
		foreach (GameObject bulletObjects in bullets)
		{
			bulletObjects.gameObject.GetComponent<MeshRenderer>().enabled = false;
		}

		//Spawn bullets to make it look like they are falling off the "bullet strip"
		//If there are more than 8 bullets left, or 8 bullets, spawn all
		if (bulletsLeft > 8 || bulletsLeft == 8) {
			spawnBullets (0);
			spawnBullets (1);
			spawnBullets (2);
			spawnBullets (3);
			spawnBullets (4);
			spawnBullets (5);
			spawnBullets (6);
			spawnBullets (7);
		}

		//Spawn the same amount of bullets as there is ammo left when reloading, only works when less than 8 ammo left
		if (bulletsLeft == 7) {	spawnBullets(7); spawnBullets(6); spawnBullets(5); spawnBullets(4); spawnBullets(3); spawnBullets(2); spawnBullets(1); } 
		if (bulletsLeft == 6) {	spawnBullets(7); spawnBullets(6); spawnBullets(5); spawnBullets(4); spawnBullets(3); spawnBullets(2);	} 
		if (bulletsLeft == 5) {	spawnBullets(7); spawnBullets(6); spawnBullets(5); spawnBullets(4); spawnBullets(3);	}
		if (bulletsLeft == 4) {	spawnBullets(7); spawnBullets(6); spawnBullets(5); spawnBullets(4);	} 
		if (bulletsLeft == 3) {	spawnBullets(7); spawnBullets(6); spawnBullets(5);	} 
		if (bulletsLeft == 2) {	spawnBullets(7); spawnBullets(6);	} 
		if (bulletsLeft == 1) {	spawnBullets(7);	} 

		//Hide the magazine
		mag.SetActive (false);

		//Instantiate the empty magazine 
		Instantiate(emptyMagPrefab, magSpawnPoint.transform.position, magSpawnPoint.transform.rotation);
		
		//Play reload animation
		holder.GetComponent<Animation>().Play("machine_gun_reload");
		//Wait for animation to finish
		yield return new WaitForSeconds (0.35f);
		//Open top part and animate mag
		top.GetComponent<Animation> ().Play ("machine_gun_top_open");
		fullMag.GetComponent<Animation>().Play("machine_gun_mag_in");
		
		//Wait for set amount of time
		yield return new WaitForSeconds(reloadDuration);
		//Refill bullets
		bulletsLeft = magazineSize;
		
		//Make the magazine visible again and play animation
		mag.SetActive (true);
		bulletHolderAnim.SetActive (true);
		bulletHolderAnim.GetComponent<Animation> ().Play ("machine_gun_bullet_holder");

		//Wait for the ammo animation to finish
		yield return new WaitForSeconds (1f);
		//Close the top lid
		top.GetComponent<Animation> ().Play ("machine_gun_top_close");
		//Wait for the top lid animation to finish
		yield return new WaitForSeconds (0.15f);
		//Slider animation
		slider.GetComponent<Animation>().Play("machine_gun_slider_eject");

		//Hide the animated bullets
		bulletHolderAnim.SetActive (false);

		//Enable shooting again
		outOfAmmo = false;
		isReloading = false;
		
		//Show the bullet belt again
		foreach (GameObject bulletObjects in bullets)
		{
			bulletObjects.gameObject.GetComponent<MeshRenderer>().enabled = true;
		}
	}	
	
	IEnumerator Muzzleflash () {
		//Chooses a random muzzleflash from the array
		sideMuzzle.GetComponent<SpriteRenderer>().sprite = muzzleflashSideSprites [Random.Range(0, muzzleflashSideSprites.Length)];
		topMuzzle.GetComponent<SpriteRenderer>().sprite = muzzleflashSideSprites [Random.Range(0, muzzleflashSideSprites.Length)];
		
		//Show the muzzleflashes
		sideMuzzle.GetComponent<SpriteRenderer>().enabled = true;
		topMuzzle.GetComponent<SpriteRenderer>().enabled = true;
		frontMuzzle.GetComponent<SpriteRenderer>().enabled = true;
		
		//Show the light
		lightFlash.GetComponent<Light>().enabled = true;
		
		//Wait for set amount of time
		yield return new WaitForSeconds(muzzleFlashDuration);
		
		//Hide the muzzleflashes
		sideMuzzle.GetComponent<SpriteRenderer>().enabled = false;
		topMuzzle.GetComponent<SpriteRenderer>().enabled = false;
		frontMuzzle.GetComponent<SpriteRenderer>().enabled = false;
		
		//Hide the light
		lightFlash.GetComponent<Light>().enabled = false;
	}	

	//Hides all bullets in the bullet belt/strip
	public void hideBullets(int num) {
		for(int i = 0; i < bullets.Length; i++) {
			if(i == num)
				bullets[i].gameObject.GetComponent<MeshRenderer>().enabled = false;
		}
	}

	//Spawn bullets in the bullet belt/strip spawnpoints
	public void spawnBullets(int num) {
		for(int o = 0; o < bullets.Length; o++) {
			if(o == num)
				//Spawn
				Instantiate(bulletPrefab, bulletSpawnpoints[o].transform.position, bulletSpawnpoints[o].transform.rotation);
		}
	}
	
	void Update () {
		//Shoot when left click is held down
		if (Input.GetMouseButton (0)) {
			if (Time.time - lastFired > 1 / fireRate && !outOfAmmo && !isReloading) {
				lastFired = Time.time;

				//Rotate the sphere holding the bullets 
				//Make it look like they are "spinning" to the right
				sphereRotate.GetComponent<Animation>().Play("machine_gun_sphere_rotate");

				//Muzzleflash
				StartCoroutine(Muzzleflash());
				
				//Play recoil animation
				holder.GetComponent<Animation>().Play("machine_gun_recoil");
				
				//Remove 1 bullet everytime you shoot
				bulletsLeft -=1;
				
				//Play smoke particles
				smokeParticles.Play();

				//Spawn casing
				Instantiate(casingPrefab, casingSpawnPoint.transform.position, casingSpawnPoint.transform.rotation);

				//Hide bullets in bullet belt when low ammo
				if (bulletsLeft == 7) {	hideBullets(7);	} 
				if (bulletsLeft == 6) {	hideBullets(6);	} 
				if (bulletsLeft == 5) {	hideBullets(5);	} 
				if (bulletsLeft == 4) {	hideBullets(4);	} 
				if (bulletsLeft == 3) {	hideBullets(3);	} 
				if (bulletsLeft == 2) {	hideBullets(2);	} 
				if (bulletsLeft == 1) {	hideBullets(1);	} 
				if (bulletsLeft == 0) {	hideBullets(0);	} 
			}
		}

		//Reload when R key is pressed, if reloaded when ammo is at 0
		if (Input.GetKeyDown(KeyCode.R) && bulletsLeft == 0 && !isReloading) {
			StartCoroutine(Reload());
		}

		//Reload when R key is pressed, if reloaded when ammo is higher than 0
		if (Input.GetKeyDown(KeyCode.R) && bulletsLeft > 0 && bulletsLeft < magazineSize && !isReloading) {
			StartCoroutine(Reload2());
		}
		
		//If out of ammo
		if (bulletsLeft == 0) {
			outOfAmmo = true;
		}
	}
}