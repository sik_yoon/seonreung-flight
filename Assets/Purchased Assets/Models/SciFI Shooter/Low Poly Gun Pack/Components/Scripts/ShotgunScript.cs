﻿using UnityEngine;
using System.Collections;

public class ShotgunScript : MonoBehaviour {

	bool outOfAmmo = false;
	bool isReloading = false;
	//Used for firerate
	float lastFired;

	[Header("Spawnpoints & Prefabs")]
	public Transform casingSpawnPoint;
	public Transform casingPrefab;
	[Header("Components")]
	public GameObject holder;
	public GameObject slider;
	public GameObject shell;
	public ParticleSystem smokeParticles;
	public GameObject sideMuzzle;
	public GameObject topMuzzle;
	[Header("Customizable Options")]
	public int magazineSize;
	int bulletsLeft;
	float reloadDuration = 4.2f;
	public float muzzleFlashDuration;
	public Light lightFlash;
	[Range(1f, 4f)]
	public float lightIntensity;
	[Range(5f, 50f)]
	public float lightRange;

	void Start () {
		//Set the magazine size
		bulletsLeft = magazineSize;
		
		//Make sure the light is off
		lightFlash.GetComponent<Light>().enabled = false;
		
		//Set the light values
		lightFlash.intensity = lightIntensity;
		lightFlash.range = lightRange;
		
		//Hide the muzzleflashes at start
		sideMuzzle.GetComponent<Renderer>().enabled = false;
		topMuzzle.GetComponent<Renderer>().enabled = false;
	}

	IEnumerator Reload () {
		//Start reloading
		isReloading = true;

		//Play reload animations
		holder.GetComponent<Animation>().Play("shotgun_main_reload_up");
		slider.GetComponent<Animation> ().Play ("shotgun_slide_back");

		//Play shell reload animation 8 times
		for(int count = 1; count <= 8; count++)
		{
			shell.GetComponent<Animation>().PlayQueued("shotgun_shell_in");
		}

		//Wait for reload duration, default 4.2
		yield return new WaitForSeconds(reloadDuration);
	
		//Refill bullets
		bulletsLeft = magazineSize;

		//Play animations and wait
		holder.GetComponent<Animation>().Play("shotgun_main_reload_down");
		slider.GetComponent<Animation> ().Play ("shotgun_slide_forward");
		yield return new WaitForSeconds (0.2f);
		slider.GetComponent<Animation> ().Play ("shotgun_slide_back");
		yield return new WaitForSeconds (0.2f);
		slider.GetComponent<Animation> ().Play ("shotgun_slide_forward");

		//Enable shooting again
		outOfAmmo = false;
		isReloading = false;
	}	
	
	IEnumerator Muzzleflash () {
		//Show the muzzleflashes
		sideMuzzle.GetComponent<SpriteRenderer>().enabled = true;
		topMuzzle.GetComponent<SpriteRenderer>().enabled = true;
		
		//Show the light
		lightFlash.GetComponent<Light>().enabled = true;
		
		//Wait for set amount of time
		yield return new WaitForSeconds(muzzleFlashDuration);
		
		//Hide the muzzleflashes
		sideMuzzle.GetComponent<SpriteRenderer>().enabled = false;
		topMuzzle.GetComponent<SpriteRenderer>().enabled = false;
		
		//Hide the light
		lightFlash.GetComponent<Light>().enabled = false;
	}	

	IEnumerator ShotgunPump () {
		//Disable shooting
		isReloading = true;

		//Wait before playing the pump animation
		yield return new WaitForSeconds (0.35f);
		slider.GetComponent<Animation> ().Play ("shotgun_slide_pump");
		//Wait for the animation to finish
		yield return new WaitForSeconds (0.08f);
		
		//Spawn shellcasing
		Instantiate(casingPrefab, casingSpawnPoint.transform.position, casingSpawnPoint.transform.rotation);

		//Wait before being able to shoot again
		yield return new WaitForSeconds (0.25f);

		//Enable shooting
		isReloading = false;
	}

	void Update () {
		//Shoot when left click is pressed
		if (Input.GetMouseButtonDown(0) && !outOfAmmo && !isReloading) {

				//Muzzleflash
				StartCoroutine(Muzzleflash());
				StartCoroutine(ShotgunPump());	

				//Play recoil animation
				holder.GetComponent<Animation>().Play("shotgun_main_recoil");
				
				//Remove 1 bullet everytime you shoot
				bulletsLeft -=1;
				
				//Play smoke particles
				smokeParticles.Play();
		}

		//Reload when R key is pressed
		if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !isReloading) {
			StartCoroutine(Reload());
		}

		//If out of ammo
		if (bulletsLeft == 0) {
			outOfAmmo = true;
		}
	}
}