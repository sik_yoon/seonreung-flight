﻿using UnityEngine;
using System.Collections;

public class ARScript : MonoBehaviour {

	bool outOfAmmo = false;
	bool isReloading = false;
	//Used for firerate
	float lastFired;

	[Header("Spawnpoints & Prefabs")]
	public Transform casingSpawnPoint;
	public Transform casingPrefab;
	public Transform magSpawnPoint;
	public Transform emptyMagPrefab;
	[Header("Components")]
	public GameObject holder;
	public GameObject slider;
	public GameObject mag;
	public GameObject fullMag;
	public ParticleSystem smokeParticles;
	public GameObject sideMuzzle;
	public GameObject topMuzzle;
	public GameObject frontMuzzle;
	public Sprite[] muzzleflashSideSprites;
	[Header("Customizable Options")]
	public int magazineSize;
	int bulletsLeft;
	float reloadDuration = 1.5f;
	public float muzzleFlashDuration;
	public float fireRate;
	public Light lightFlash;
	[Range(1f, 4f)]
	public float lightIntensity;
	[Range(5f, 50f)]
	public float lightRange;
	
	void Start () {
		//Set the magazine size
		bulletsLeft = magazineSize;
		
		//Make sure the light is off
		lightFlash.GetComponent<Light>().enabled = false;
		
		//Set the light values
		lightFlash.intensity = lightIntensity;
		lightFlash.range = lightRange;
		
		//Hide the muzzleflashes at start
		sideMuzzle.GetComponent<Renderer>().enabled = false;
		topMuzzle.GetComponent<Renderer>().enabled = false;
		frontMuzzle.GetComponent<SpriteRenderer>().enabled = false;
	}
	
	IEnumerator Reload () {
		//Start reloading
		isReloading = true;
		
		//Hide the magazine
		mag.GetComponent<MeshRenderer>().enabled = false;
		
		//Spawn empty magazine
		Instantiate(emptyMagPrefab, magSpawnPoint.transform.position, magSpawnPoint.transform.rotation);
		
		//Play reload and mag in animation
		holder.GetComponent<Animation>().Play("ar_main_reload");
		fullMag.GetComponent<Animation> ().Play ("ar_mag_in");

		//Wait for set amount of time
		yield return new WaitForSeconds(reloadDuration);
		
		//Refill bullets
		bulletsLeft = magazineSize;
		
		//Make the magazine visible again
		mag.GetComponent<MeshRenderer>().enabled = true;

		//Wait for the slider animation to finish
		yield return new WaitForSeconds (0.25f);

		slider.GetComponent<Animation>().Play("ar_slider_eject");

		//Enable shooting again
		outOfAmmo = false;
		isReloading = false;
	}	
	
	IEnumerator Muzzleflash () {
		//Chooses a random muzzleflash from the array
		sideMuzzle.GetComponent<SpriteRenderer>().sprite = muzzleflashSideSprites [Random.Range(0, muzzleflashSideSprites.Length)];
		topMuzzle.GetComponent<SpriteRenderer>().sprite = muzzleflashSideSprites [Random.Range(0, muzzleflashSideSprites.Length)];
		
		//Show the muzzleflashes
		sideMuzzle.GetComponent<SpriteRenderer>().enabled = true;
		topMuzzle.GetComponent<SpriteRenderer>().enabled = true;
		frontMuzzle.GetComponent<SpriteRenderer>().enabled = true;
		
		//Show the light
		lightFlash.GetComponent<Light>().enabled = true;
		
		//Wait for set amount of time
		yield return new WaitForSeconds(muzzleFlashDuration);
		
		//Hide the muzzleflashes
		sideMuzzle.GetComponent<SpriteRenderer>().enabled = false;
		topMuzzle.GetComponent<SpriteRenderer>().enabled = false;
		frontMuzzle.GetComponent<SpriteRenderer>().enabled = false;
		
		//Hide the light
		lightFlash.GetComponent<Light>().enabled = false;
	}	
	
	void Update () {
		//Shoot when left click is held down
		if (Input.GetMouseButton (0)) {
			if (Time.time - lastFired > 1 / fireRate && !outOfAmmo && !isReloading) {
				lastFired = Time.time;

				//Muzzleflash
				StartCoroutine(Muzzleflash());
				
				//Play recoil and eject animations
				holder.GetComponent<Animation>().Play("ar_recoil");
				slider.GetComponent<Animation>().Play("ar_slider");
				
				//Remove 1 bullet everytime you shoot
				bulletsLeft -=1;
				
				//Play smoke particles
				smokeParticles.Play();
			
				//Spawn casing
				Instantiate(casingPrefab, casingSpawnPoint.transform.position, casingSpawnPoint.transform.rotation);
			}
		}

		//Reload when R key is pressed
		if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !isReloading) {
			StartCoroutine(Reload());
		}
		
		//If out of ammo
		if (bulletsLeft == 0) {
			outOfAmmo = true;
	}}
}