﻿using UnityEngine;
using System.Collections;

public class HandgunScript : MonoBehaviour {

	bool hasPlayed = false;
	bool outOfAmmoSlider = false;
	bool outOfAmmo = false;
	bool isReloading = false;

	[Header("Spawnpoints & Prefabs")]
	public Transform casingSpawnPoint;
	public Transform casingPrefab;
	public Transform magSpawnPoint;
	public Transform emptyMagPrefab;
	[Header("Components")]
	public GameObject holder;
	public GameObject slide;
	public GameObject mag;
	public GameObject fullMag;
	public ParticleSystem smokeParticles;
	public GameObject sideMuzzle;
    public GameObject topMuzzle;
	public GameObject frontMuzzle;
	public Sprite[] muzzleflashSideSprites;
	[Header("Customizable Options")]
	public int magazineSize;
	int bulletsLeft;
	float reloadDuration = 1.5f;
	public float muzzleFlashDuration;
	public Light lightFlash;
    [Range(1f, 4f)]
    public float lightIntensity;
    [Range(5f, 50f)]
    public float lightRange;
	
	void Start () {
		//Set the magazine size
		bulletsLeft = magazineSize;

		//Make sure the light is off
		lightFlash.GetComponent<Light>().enabled = false;

		//Set the light values
		lightFlash.intensity = lightIntensity;
		lightFlash.range = lightRange;

		//Hide the muzzleflashes at start
        sideMuzzle.GetComponent<Renderer>().enabled = false;
        topMuzzle.GetComponent<Renderer>().enabled = false;
		frontMuzzle.GetComponent<SpriteRenderer>().enabled = false;
	}
	
	IEnumerator Reload () {
		//Start reloading
		isReloading = true;

		//Hide the magazine
		mag.GetComponent<MeshRenderer>().enabled = false;

		//Spawn empty magazine
		Instantiate(emptyMagPrefab, magSpawnPoint.transform.position, magSpawnPoint.transform.rotation);

		//Play reload and mag in animation
		holder.GetComponent<Animation>().Play("handgun_main_reload");
		fullMag.GetComponent<Animation>().Play("handgun_full_mag_in");

		//Wait for set amount of time
		yield return new WaitForSeconds(reloadDuration);
		
		//Refill bullets
		bulletsLeft = magazineSize;

		//Make the magazine visible again
		mag.GetComponent<MeshRenderer>().enabled = true;

		//Enable shooting again
		outOfAmmo = false;
		isReloading = false;
		hasPlayed = false;

		//Only play this if gun ran out of bullets before reloading
		if (outOfAmmoSlider == true) {
			slide.GetComponent<Animation>().Play("handgun_full_ammo");
			outOfAmmoSlider = false;
		} else {
			slide.GetComponent<Animation>().Play("handgun_slide_reload");
		}
	}	
	
	IEnumerator Muzzleflash () {
		//Chooses a random muzzleflash from the array
        sideMuzzle.GetComponent<SpriteRenderer>().sprite = muzzleflashSideSprites [Random.Range(0, muzzleflashSideSprites.Length)];
        topMuzzle.GetComponent<SpriteRenderer>().sprite = muzzleflashSideSprites [Random.Range(0, muzzleflashSideSprites.Length)];
		
		//Show the muzzleflashes
		sideMuzzle.GetComponent<SpriteRenderer>().enabled = true;
        topMuzzle.GetComponent<SpriteRenderer>().enabled = true;
		frontMuzzle.GetComponent<SpriteRenderer>().enabled = true;

		//Show the light
		lightFlash.GetComponent<Light>().enabled = true;

		//Wait for set amount of time
		yield return new WaitForSeconds(muzzleFlashDuration);
		
		//Hide the muzzleflashes
		sideMuzzle.GetComponent<SpriteRenderer>().enabled = false;
        topMuzzle.GetComponent<SpriteRenderer>().enabled = false;
		frontMuzzle.GetComponent<SpriteRenderer>().enabled = false;

		//Hide the light
		lightFlash.GetComponent<Light>().enabled = false;
	}	
	
	void Update () {
		//Shoot when left click is pressed
		if (Input.GetMouseButtonDown(0) && !outOfAmmo && !isReloading) {

			//Muzzleflash
			StartCoroutine(Muzzleflash());

			//Play recoil animations
			holder.GetComponent<Animation>().Play("handgun_main_recoil");
			slide.GetComponent<Animation>().Play("handgun_blowback");
			
			//Remove 1 bullet everytime you shoot
			bulletsLeft -=1;
			
			//Play smoke particles
			smokeParticles.Play();

			//Spawn casing
			Instantiate(casingPrefab, casingSpawnPoint.transform.position, casingSpawnPoint.transform.rotation);
		}

		//Reload when R key is pressed
		if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !isReloading) {
			StartCoroutine(Reload());
		}
		
		//If out of ammo
		if (bulletsLeft == 0) {
			outOfAmmo = true;
				//Play the slider animation once only
				if (!hasPlayed) {
					slide.GetComponent<Animation>().Play("handgun_out_of_ammo");
					hasPlayed = true;
					outOfAmmoSlider = true;
				}
		}
	}
}