﻿using UnityEngine;
using System.Collections;

public class RPGScript : MonoBehaviour {

	bool isReloading = false;
	//Used for firerate
	float lastFired;
	//Inspector
	[Header("Spawnpoints & Prefabs")]
	public Transform projectileSpawnPoint;
	public Transform projectilePrefab;
	[Header("Components")]
	public GameObject holder;
	public GameObject projectile;
	public ParticleSystem smokeParticlesFront;
	public ParticleSystem smokeParticlesBack;
	[Header("Customizable Options")]
	float reloadDuration = 2f;
	public float muzzleFlashDuration;
	public Light lightFlash;
	public Light lightFlashBack;
	[Range(1f, 4f)]
	public float lightIntensity;
	[Range(5f, 50f)]
	public float lightRange;

	void Start () {
		//Make sure the light is off
		lightFlash.GetComponent<Light>().enabled = false;
		lightFlashBack.GetComponent<Light>().enabled = false;
		
		//Set the light values
		lightFlash.intensity = lightIntensity;
		lightFlash.range = lightRange;
		lightFlashBack.intensity = lightIntensity;
		lightFlashBack.range = lightRange;
	}

	IEnumerator Reload () {
		//Start reloading
		isReloading = true;
		//Hide the projectile and play the reload animation
		projectile.GetComponent<MeshRenderer> ().enabled = false;
		projectile.GetComponent<Animation> ().Play ("rpg_projectile_reload");

		//Wait for reload duration, default 4.2
		yield return new WaitForSeconds(reloadDuration);

		//Enable shooting again
		isReloading = false;
		projectile.GetComponent<MeshRenderer> ().enabled = true;
	}	
	
	IEnumerator Muzzleflash () {
		//Show the light
		lightFlash.GetComponent<Light>().enabled = true;
		lightFlashBack.GetComponent<Light>().enabled = true;
		
		//Wait for set amount of time
		yield return new WaitForSeconds(muzzleFlashDuration);
		
		//Hide the light
		lightFlash.GetComponent<Light>().enabled = false;
		lightFlashBack.GetComponent<Light>().enabled = false;
	}	

	void Update () {
		//Shoot when left click is pressed
		if (Input.GetMouseButtonDown(0) && !isReloading) {
				
				//Reload and show muzzleflash
				StartCoroutine(Muzzleflash());
				StartCoroutine(Reload ());

				//Play recoil animation
				holder.GetComponent<Animation>().Play("rpg_recoil");
				
				//Instantiate the projectile
				Instantiate(projectilePrefab, projectileSpawnPoint.transform.position, projectileSpawnPoint.transform.rotation);

				//Play smoke particles
				smokeParticlesFront.Play();
				smokeParticlesBack.Play();
	}}
}