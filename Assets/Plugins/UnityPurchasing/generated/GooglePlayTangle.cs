#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("aNpZemhVXlFy3hDer1VZWVldWFv1LTIGokPWhhrpBb/3ElILO+MXPxfdUajqEFPH1rqsDQ0NvLATqsV4JAxwCNDHSXUUG6oZh6NIpxRKKX7pd1cpWvZ+SwIh4IX3TMW9mLPp+E+6IgmbiTrAVxRBpIyUv+jGin/Il6zs7n3IMUqBtL0CxUTxP+y4IAjD+Q2wOKm770rU2alhRq0C9juYD9pZV1ho2llSWtpZWViAphHwy9m9ER55FN6zNcgHGkl9TCcIXQ+5ARHvjpiZma5KI9jW9Ln0DJl8GoYotn6BAe2yl9+D1hFwSi+uPEn7Ur7WmB+puqPCtxtpByS9DSuiHS5b+36+4n+Aaabi1UkbKdscPWgUHAandcjx3LRe8kakuVpbWVhZ");
        private static int[] order = new int[] { 0,11,4,3,12,9,13,7,11,10,11,12,12,13,14 };
        private static int key = 88;

        public static byte[] Data() {
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
