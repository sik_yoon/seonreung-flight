#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("r8EndsbM/onfsZ6HgtScooWZsZeEgYIDgI6BsQOAi4MDgICBZRAoiLSzsLWxsrfbloyytLGzsbizsLWx7+Wh4u7v5ej16O7v8qHu56H08uTYJoSI/ZbB15Cf9VI2CqK6xiJU7vuxA4D3sY+HgtScjoCAfoWFgoOA9ejn6OLg9eSh4/ih4O/4ofHg8/XIWfcespXkIPYVSKyDgoCBgCIDgJ4QWp/G0WqEbN/4BaxqtyPWzdRt/sApGXhQS+cdpeqQUSI6ZZqrQp42mjwSw6WTq0aOnDfMHd/iScoBlrK327HjsIqxiIeC1IWHkoPU0rCSNLssdY6PgROKMKCXr/VUvYxa45eMh4irB8kHdoyAgISEgYIDgICB3e3kocjv4q+wp7Glh4LUhYqSnMDx5bSilMqU2JwyFXZ3HR9O0TtA2dGXsZWHgtSFgpKMwPHx7eSh0+7u9fHt5KHC5PP16Ofo4uD16O7vocD0sZCHgtSFi5KLwPHx7eShyO/ir7CtoeLk8/Xo5+ji4PXkofHu7eji+LEDhTqxA4IiIYKDgIODgIOxjIeIAZWqUejGFfeIf3XqDK/BJ3bGzP63GM2s+TZsDRpdcvYac/dT9rHOQFi3/kAG1FgmGDizw3pZVPAf/yDTMLHZbduFsw3pMg6cX+Tyfubf5D329q/g8fHt5K/i7uyu4PHx7eTi4IWHkoPU0rCSsZCHgtSFi5KLwPHxCpgIX3jK7XSGKqOxg2mZv3nRiFIpXf+jtEukVFiOV+pVI6WikHYgLYmqh4CEhIaDgJef6fX18fK7rq72hm38uAIK0qFSuUUwPhvOi+p+qn1ImPN03I9U/t4ac6SCO9QOzNyMcIeC1JyPhZeFlapR6MYV94h/deoMqwfJB3aMgICEhIGx47CKsYiHgtSh4O/loeLk8/Xo5+ji4PXo7u+h8fHt5KHT7u71ocLAsZ+WjLG3sbWz9enu8+j1+LCXsZWHgtSFgpKMwPHE/57N6tEXwAhF9eOKkQLABrILAOjn6OLg9eju76HA9PXp7vPo9fiwpWNqUDbxXo7EYKZLcOz5bGY0lpaJ37EDgJCHgtScoYUDgImxA4CFsYexjoeC1JySgIB+hYSxgoCAfrGcoe7nofXp5KH16eTvoeDx8e3o4uD4oeDy8vTs5PKh4OLi5PH14O/i5D918hpvU+WOSvjOtVkjv3j5fupJ5g6JNaF2Si2toe7xN76AsQ02wk68p+ahC7LrdowDTl9qIq540uva5Y4cvHKqyKmbSX9PNDiPWN+dV0q8p7Glh4LUhYqSnMDx8e3kocLk8/WhwsCxA4CjsYyHiKsHyQd2jICAgOPt5KHy9eDv5eDz5aH15PPs8qHgA4CBh4irB8kHduLlhICxAHOxq4cO8gDhR5raiK4TM3nFyXHhuR+UdEHisvZ2u4at12pbjqCPWzvymM40KiLwE8bS1EAursAyeXpi8UxnIs0UH/uNJcYK2lWXtrJKRY7MT5XoUK6xAEKHiaqHgISEhoODsQA3mwAyngQCBJoYvMa2cygawQ+tVTARk1nz4OL16OLkofL14PXk7OTv9fKvsdPk7ejg7+Lkoe7vofXp6PKh4uTz0SsLVFtlfVGIhrYx9PSg");
        private static int[] order = new int[] { 31,14,7,43,52,28,39,27,15,17,59,15,23,47,43,20,23,52,52,44,31,32,48,58,26,36,27,58,54,35,51,44,51,43,45,39,54,54,44,53,43,53,53,43,50,51,56,48,59,59,53,51,55,57,58,55,58,59,58,59,60 };
        private static int key = 129;

        public static byte[] Data() {
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
