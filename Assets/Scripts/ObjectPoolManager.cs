﻿using UnityEngine;
using System.Collections.Generic;

public class ObjectPoolManager : MonoBehaviour
{
    public static ObjectPoolManager instance;
    private Dictionary<string, GameObject> poolablePrefabs = new Dictionary<string, GameObject>();
    private Dictionary<string, Stack<GameObject>> poolTable = new Dictionary<string, Stack<GameObject>>();
    private List<GameObject> usingObjs = new List<GameObject>();

    void Awake()
    {
        instance = this;

        GetResourcesAtPath("Zombies");
        GetResourcesAtPath("Enemies");
        GetResourcesAtPath("Bullets");
        // GetResourcesAtPath("Particles");
    }

    void Start()
    {
        CreateObjectPool();
    }

    private void GetResourcesAtPath(string path)
    {
        Object[] objs;
        objs = Resources.LoadAll(path);

        for(int i = 0 ; i < objs.Length ; i++)
        {
            GameObject obj = (GameObject)objs[i];
            poolablePrefabs[obj.name] = obj;
        }
    }

    private void CreateObjectPool()
    {
        foreach(GameObject prefab in poolablePrefabs.Values)
        {
            if(poolTable.ContainsKey(prefab.name))
            {
                Debug.Log("already exist " + prefab.name + " pool.");
                return;
            }

            Stack<GameObject> pool = new Stack<GameObject>();
            for(int i = 0 ; i < 10 ; i++)
                pool.Push(InstantiatePrefab(prefab));
            poolTable[prefab.name] = pool;
        }
    }

    private GameObject InstantiatePrefab(GameObject prefab)
    {
        GameObject obj = Instantiate(prefab);
        obj.name = prefab.name;
        obj.transform.parent = transform;
        obj.SetActive(false);

        return obj;
    }

    private GameObject GetPooledObject(string poolName)
    {
        if(!poolTable.ContainsKey(poolName))
        {
            Debug.Log(poolName + " pool doesn't exist.");
            return null;
        }

        Stack<GameObject> pool = poolTable[poolName];
        GameObject obj;
        if(pool.Count == 0)
            obj = InstantiatePrefab(poolablePrefabs[poolName]);
        else obj = pool.Pop();

        usingObjs.Add(obj);

        return obj;
    }

    public GameObject GetPooledObject(string poolName, Vector3 position)
    {
        GameObject obj = GetPooledObject(poolName);
        obj.transform.position = position;
        obj.transform.rotation = Quaternion.Euler(Vector3.zero);

        return obj;
    }

    public GameObject GetPooledObject(string poolName, Vector3 position, Quaternion rotation)
    {
        GameObject obj = GetPooledObject(poolName);
        obj.transform.position = position;
        obj.transform.rotation = rotation;

        return obj;
    }

    public GameObject GetPooledObject(string poolName, Transform transform)
    {
        GameObject obj = GetPooledObject(poolName);
        obj.transform.position = transform.position;
        obj.transform.rotation = transform.rotation;

        return obj;
    }

    public void ReturnPooledObject(GameObject obj)
    {
        if(!poolTable.ContainsKey(obj.name))
        {
            Debug.Log(obj.name + " pool doesn't exist.");
            return;
        }

        obj.SetActive(false);
        poolTable[obj.name].Push(obj);
        usingObjs.Remove(obj);
    }

    public void ReturnAllPooledObjects()
    {
        for(int i = usingObjs.Count - 1 ; i >= 0 ; --i)
            ReturnPooledObject(usingObjs[i]);
    }
}
