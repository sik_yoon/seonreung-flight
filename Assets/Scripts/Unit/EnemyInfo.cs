﻿
public struct EnemyInfo
{
    public float hp;
    public float moveSpeed;
    public float scale;
    public string[] modelNames;

    public EnemyInfo(float hp, float moveSpeed, float scale, int modelNum)
    {
        this.hp = hp;
        this.moveSpeed = moveSpeed;
        this.scale = scale;
        this.modelNames = new string[modelNum];
    }
}
