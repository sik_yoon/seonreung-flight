﻿using UnityEngine;
using System.Collections;

public abstract class EnemyCtrl : UnitCtrl
{
    protected Transform childTr;

    private bool isAttackedEffecting;

    protected int hp;
    protected float moveSpeed;

    protected override void Init()
    {
        base.Init();

        childTr = tr.GetChild(0).GetComponent<Transform>();

        tag = "Enemy";
        gameObject.layer = 9;
    }

    public override void ResetStatus()
    {
        base.ResetStatus();

        isAttackedEffecting = false;
    }

    public void SetStatus(int hp, float moveSpeed, float scale)
    {
        this.hp = hp;
        this.moveSpeed = moveSpeed;

        childTr.localScale = new Vector3(scale, scale, scale);
        coll.radius = scale * 0.8f;
    }

    public virtual void Attacked(int damage)
    {
        if(!isAttackedEffecting) StartCoroutine(AttackedRoutine());

        hp -= damage;

        if(hp <= 0) StartCoroutine(DieRoutine());
    }

    protected abstract IEnumerator AIRoutine();

    protected abstract int GetDropGoldAmount();

    protected IEnumerator MoveRoutine(float time)
    {
        for(float t = 0 ; isAlive && t < time ; t += Time.deltaTime)
        {
            if(tr.position.z < -5)
            {
                PlayerCtrl.instance.Attacked();
                Disappear();
                break;
            }

            tr.Translate(Vector3.forward * Time.deltaTime * moveSpeed, Space.Self);

            yield return null;
        }
    }

    protected IEnumerator TrackMoveRoutine(float time)
    {
        float dx, dy, degree, prevDegree = 180;
        float rotateSpeed = moveSpeed * 0.1f;
        for(float t = 0 ; isAlive && t < time ; t += Time.deltaTime)
        {
            if(tr.position.z < -5)
            {
                PlayerCtrl.instance.Attacked();
                Disappear();
                break;
            }

            dx = PlayerCtrl.instance.tr.position.x - tr.position.x;
            dy = -tr.position.z;
            degree = (Mathf.Atan2(dx, dy) * Mathf.Rad2Deg + 360) % 360;
            if(degree > prevDegree) degree = prevDegree + rotateSpeed;
            else if(degree < prevDegree) degree = prevDegree - rotateSpeed;

            tr.localRotation = Quaternion.Euler(0, degree, 0);
            tr.Translate(Vector3.forward * Time.deltaTime * moveSpeed, Space.Self);

            yield return null;

            prevDegree = degree;
        }
    }

    protected virtual IEnumerator DieRoutine()
    {
        SoundManager.instance.PlayEnemyDieSound();
        GameManager.instance.EarnGold(GetDropGoldAmount());

        isAlive = false;
        coll.enabled = false;

        yield return new WaitForSeconds(2.0f);

        SpawnManager.instance.DiscountEnemiesLeft();
        Disappear();
    }

    protected virtual void Disappear()
    {
        ObjectPoolManager.instance.ReturnPooledObject(gameObject);
    }

    IEnumerator AttackedRoutine()
    {
        isAttackedEffecting = true;

        SetMaterialsColor(Color.red);
        yield return new WaitForSeconds(0.15f);
        SetMaterialsColor(Color.white);

        isAttackedEffecting = false;
    }
}
