﻿using UnityEngine;
using System.Collections;
using System;

public class WeaponEnemyCtrl : EnemyCtrl
{
    public Weapon weapon;
    protected Animator anim;

    protected override void Init()
    {
        base.Init();

        anim = GetComponentInChildren<Animator>();
    }

    public override void ResetStatus()
    {
        base.ResetStatus();

        anim.SetTrigger("Reset_t");
    }

    IEnumerator ShootRoutine()
    {
        anim.SetTrigger("Shoot_t");

        yield return new WaitForSeconds(0.2f);

        if(isAlive)
        {
            yield return StartCoroutine(weapon.ShootRoutine());

            yield return new WaitForSeconds(weapon.GetCurrentReloadTime());
        }
    }

    protected override IEnumerator AIRoutine()
    {
        while(isAlive)
        {
            anim.SetBool("Move_b", true);
            yield return StartCoroutine(MoveRoutine(2.0f));
            anim.SetBool("Move_b", false);

            yield return StartCoroutine(ShootRoutine());
        }
    }

    protected override IEnumerator DieRoutine()
    {
        anim.SetTrigger("Die_t");

        yield return StartCoroutine(base.DieRoutine());
    }

    void Awake()
    {
        Init();
    }

    void OnEnable()
    {
        ResetStatus();

        StartCoroutine(AIRoutine());
    }

    void OnTriggerEnter(Collider coll)
    {
        if(coll.CompareTag("Player")) PlayerCtrl.instance.Attacked();
    }

    protected override int GetDropGoldAmount()
    {
        return hp;
    }
}
