﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public abstract class UnitCtrl : MonoBehaviour
{
    [HideInInspector]
    public Transform tr;
    protected Material[] materials;
    protected CapsuleCollider coll;

    protected bool isAlive;

    protected virtual void Init()
    {
        tr = GetComponent<Transform>();
        coll = GetComponent<CapsuleCollider>();

        Renderer[] renderers = GetComponentsInChildren<Renderer>();
        materials = new Material[renderers.Length];
        for(int i = 0 ; i < renderers.Length ; i++)
            materials[i] = renderers[i].material;
    }

    public virtual void ResetStatus()
    {
        isAlive = true;
        coll.enabled = true;

        SetMaterialsColor(Color.white);
    }

    protected void SetMaterialsColor(Color color)
    {
        for(int i = 0 ; i < materials.Length ; i++)
            materials[i].color = color;
    }
}
