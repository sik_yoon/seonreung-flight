﻿using UnityEngine;
using System.Collections;

public class CharacterCtrl : MonoBehaviour
{
    private Weapon weapon;
    private Animator anim;
    private AudioSource dieSound;

    [HideInInspector]
    public Vector3 relativePos;

    public int weaponType;
    public float headHorizontalRotate;
    public float headVerticalRotate;
    public float bodyHorizontalRotate;
    public float bodyVerticalRotate;

    void Awake()
    {
        weapon = GetComponent<Weapon>();

        relativePos = -transform.position;

        anim = GetComponentInChildren<Animator>();
    }

    void OnEnable()
    {
        SetAnimToShoot();
    }

    public Weapon GetWeapon()
    {
        return weapon;
    }

    public void SetAnimToDie()
    {
        anim.SetBool("Death_b", true);
        anim.SetBool("Shoot_b", false);
    }

    public void SetAnimToShoot()
    {
        anim.SetInteger("WeaponType_int", weaponType);
        anim.SetFloat("Head_Horizontal_f", headHorizontalRotate);
        anim.SetFloat("Head_Vertical_f", headVerticalRotate);
        anim.SetFloat("Body_Horizontal_f", bodyHorizontalRotate);
        anim.SetFloat("Body_Vertical_f", bodyVerticalRotate);

        anim.SetBool("Shoot_b", true);
        anim.SetBool("Death_b", false);
    }

    public void PlayDieSound()
    {
        dieSound.Play();
    }
}
