﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerCtrl : UnitCtrl
{
    public static PlayerCtrl instance;

    private Dictionary<string, CharacterCtrl> characters = new Dictionary<string, CharacterCtrl>();
    private CharacterCtrl selectedCharacter;
    private Transform charactersTr;

    private bool isShooting;
    private IEnumerator shootRoutine;

    private MoveSliderCtrl moveSlider;

    public float maxXPos = 12;

    protected override void Init()
    {
        base.Init();

        tag = "Player";
        gameObject.layer = 8;

        charactersTr = tr.GetChild(0);
        for(int i = 0 ; i < charactersTr.childCount ; i++)
        {
            selectedCharacter = charactersTr.GetChild(i).GetComponent<CharacterCtrl>();

            characters[selectedCharacter.name] = selectedCharacter;
        }

        moveSlider = UIManager.instance.moveSlider;
    }

    public override void ResetStatus()
    {
        base.ResetStatus();

        selectedCharacter.SetAnimToShoot();
        StartShoot();
    }

    public void FocusCharacter(string characterName)
    {
        selectedCharacter = characters[characterName];

        tr.position = Vector3.zero;
        charactersTr.gameObject.MoveTo(selectedCharacter.relativePos, 1, 0);
        // charactersTr.localPosition = selectedCharacter.relativePos;
    }

    public void ActiveAllCharacters()
    {
        foreach(CharacterCtrl character in characters.Values)
        {
            character.gameObject.SetActive(true);
        }
    }

    public void ChangeCharacterToSelected()
    {
        foreach(CharacterCtrl character in characters.Values)
        {
            if(character.name == GameManager.instance.GetSelectedCharacterName())
                selectedCharacter = character;
            else character.gameObject.SetActive(false);
        }

        FocusCharacter(selectedCharacter.name);
    }

    public void StartShoot()
    {
        if(isShooting) return;

        isShooting = true;
        shootRoutine = ShootRoutine();
        StartCoroutine(shootRoutine);
    }

    public void StartPreviewShoot()
    {
        if(isShooting) return;

        isShooting = true;
        shootRoutine = PreviewShootRoutine();
        StartCoroutine(shootRoutine);
    }

    public void StopShoot()
    {
        isShooting = false;

        selectedCharacter.GetWeapon().StopShoot();
        StopCoroutine(shootRoutine);
    }

    IEnumerator ShootRoutine()
    {
        yield return new WaitForSeconds(1.0f);
        selectedCharacter.GetWeapon().StartShoot();

        while(true)
        {
            yield return StartCoroutine(selectedCharacter.GetWeapon().ShootRoutine());
        }
    }

    IEnumerator PreviewShootRoutine()
    {
        yield return new WaitForSeconds(1.0f);
        selectedCharacter.GetWeapon().StartShoot();

        while(true)
        {
            yield return StartCoroutine(selectedCharacter.GetWeapon().PreviewShootRoutine());
        }
    }

    public void StartMove()
    {
        StartCoroutine(MoveRoutine());
    }

    IEnumerator MoveRoutine()
    {
        while(isAlive)
        {
            tr.position = new Vector3(moveSlider.GetValue(), 0, 0);
            yield return null;
        }
    }

    private IEnumerator DieRoutine()
    {
        isAlive = false;
        coll.enabled = false;

        StopShoot();

        selectedCharacter.SetAnimToDie();

        moveSlider.SetInteractable(false);

        yield return new WaitForSeconds(2.0f);

        GameManager.instance.GameOver();
    }

    public Weapon GetWeapon(string name)
    {
        return characters[name].GetWeapon();
    }

    public int GetCharacterNum()
    {
        return characters.Count;
    }

    public void Attacked()
    {
        if(isAlive) StartCoroutine(DieRoutine());
    }

    public bool IsAlive()
    {
        return isAlive;
    }

    public bool IsShooting()
    {
        return isShooting;
    }

    void Awake()
    {
        instance = this;

        Init();
    }
}
