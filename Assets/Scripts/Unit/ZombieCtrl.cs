﻿using UnityEngine;
using System.Collections;

public class ZombieCtrl : EnemyCtrl
{
    protected Animator anim;

    protected override void Init()
    {
        base.Init();

        anim = GetComponentInChildren<Animator>();
    }

    public override void ResetStatus()
    {
        base.ResetStatus();

        anim.speed = moveSpeed * 0.9f;
        anim.SetTrigger("Reset_t");
    }

    protected override IEnumerator AIRoutine()
    {
        while(isAlive)
        {
            if(Random.Range(0, 10) > 3)
                yield return StartCoroutine(MoveRoutine(20f));
            else
                yield return StartCoroutine(TrackMoveRoutine(20f));
        }
    }

    protected override IEnumerator DieRoutine()
    {
        anim.speed = 1;
        anim.SetTrigger("Die_t");

        yield return StartCoroutine(base.DieRoutine());
    }

    void Awake()
    {
        Init();
    }

    void OnEnable()
    {
        ResetStatus();

        StartCoroutine(AIRoutine());
    }

    void OnTriggerEnter(Collider coll)
    {
        if(coll.CompareTag("Player")) PlayerCtrl.instance.Attacked();
    }

    protected override int GetDropGoldAmount()
    {
        return 3;
    }
}
