﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StatusText : MonoBehaviour
{
    public static Color increaseColor = new Color32(0, 255, 0, 255);
    public static Color decreaseColor = new Color32(255, 0, 0, 255);

    public Text beforeText;
    public Text afterText;

    public void SetText(float before, float after)
    {
        beforeText.text = before == 100 ? "X" : before.ToString("N2");
        afterText.text = after == 100 ? "MAX" : after.ToString("N2");
        afterText.color = (after == 100 || before.Equals(after)) ? Color.white : before > after ? increaseColor : decreaseColor;
    }

    public void SetText(int before, int after)
    {
        beforeText.text = before == 0 ? "X" : before.ToString();
        afterText.text = after == 0 ? "MAX" : after.ToString();
        afterText.color = (after == 0 || before == after) ? Color.white : before < after ? increaseColor : decreaseColor;
    }
}
