﻿using UnityEngine;

public class SelectCharacterUI : MonoBehaviour
{
    public Transform characterListTr;

    [HideInInspector]
    public CharacterListItem currentItem;
    private CharacterListItem[] items;

    void Awake()
    {
        items = new CharacterListItem[PlayerCtrl.instance.GetCharacterNum()];
        for(int i = 0 ; i < items.Length ; ++i)
        {
            items[i] = characterListTr.GetChild(i).GetComponent<CharacterListItem>();
        }
    }

    public void CheckItems()
    {
        for(int i = 0 ; i < items.Length ; i++)
        {
            items[i].CheckButtons();
        }
    }
}
