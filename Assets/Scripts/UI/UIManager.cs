﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    public LobbyUI lobbyUI;
    public InGameUI inGameUI;
    public SelectCharacterUI selectCharacterUI;

    public MoveSliderCtrl moveSlider;
    public GameObject gold;
    public Text goldText;

    void Awake()
    {
        instance = this;

        gold.SetActive(true);
    }

    public void ChangeUIToInGame()
    {
        lobbyUI.gameObject.SetActive(false);

        inGameUI.gameObject.SetActive(true);
        moveSlider.gameObject.SetActive(true);

        SetGoldText(0);
    }

    public void ChangeUIToLobby()
    {
        inGameUI.gameObject.SetActive(false);
        selectCharacterUI.gameObject.SetActive(false);
        moveSlider.gameObject.SetActive(false);

        lobbyUI.gameObject.SetActive(true);
        CameraCtrl.instance.ChangeCameraToLobby();
        PlayerCtrl.instance.ChangeCharacterToSelected();

        SetGoldText(GameManager.instance.GetGoldAmount());
    }

    public void ChangeUIToSelectCharacter()
    {
        lobbyUI.gameObject.SetActive(false);

        selectCharacterUI.gameObject.SetActive(true);
        CameraCtrl.instance.ChangeCameraToSelectCharacter();
        PlayerCtrl.instance.ActiveAllCharacters();
    }

    public void SetGoldText(int amount)
    {
        goldText.text = amount.ToString();
    }
}
