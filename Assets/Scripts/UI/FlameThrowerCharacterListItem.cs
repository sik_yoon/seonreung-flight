﻿using UnityEngine;
using System.Collections;

public class FlameThrowerCharacterListItem : CharacterListItem
{
	private FlameThrowerWeapon flameThrower;

    protected override void SetStatus()
    {
        if(flameThrower == null) flameThrower = (FlameThrowerWeapon)weapon;

        int upgradeLevel = flameThrower.GetUpgradeLevel();
        int maxUpgradeLevel = flameThrower.GetMaxUpgradeLevel();

        int currentDamage, nextDamage;
        float currentReloadTime, nextReloadTime;
        int currentAttackRange, nextAttackRange;

        if(upgradeLevel == 0)
        {
            currentDamage = 0;
            currentReloadTime = 100;
            currentAttackRange = 0;
        }
        else
        {
            currentDamage = flameThrower.GetCurrentDamage();
            currentReloadTime = flameThrower.GetCurrentReloadTime();
            currentAttackRange = flameThrower.GetCurrentAttackRange();
        }

        if(upgradeLevel == maxUpgradeLevel)
        {
            nextDamage = 0;
            nextReloadTime = 100;
            nextAttackRange = 0;
        }
        else
        {
            nextDamage = flameThrower.GetNextDamage();
            nextReloadTime = flameThrower.GetNextReloadTime();
            nextAttackRange = flameThrower.GetNextAttackRange();
        }

        status1Text.SetText(currentDamage, nextDamage);
        status2Text.SetText(currentReloadTime, nextReloadTime);
        status3Text.SetText(currentAttackRange, nextAttackRange);
    }

    void Awake()
    {
        Init();
    }

    void OnEnable()
    {
        CheckButtons();
    }
}
