﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoveSliderCtrl : MonoBehaviour
{
    private Slider slider;
    private RectTransform tr;

    void Awake()
    {
        slider = GetComponent<Slider>();
        tr = slider.GetComponent<RectTransform>();

        slider.maxValue = PlayerCtrl.instance.maxXPos;
        slider.minValue = -slider.maxValue;

        tr.sizeDelta = new Vector2(
            (Camera.main.WorldToScreenPoint(new Vector3(PlayerCtrl.instance.maxXPos, 0, 0)).x * 2 - Camera.main.pixelWidth) / Camera.main.pixelWidth * 1920 * 1.05f,
            tr.sizeDelta.y
        );
    }

    public void SetInteractable(bool flag)
    {
        slider.interactable = flag;
    }

    public void SetValueToZero()
    {
        slider.value = 0;
    }

    public float GetValue()
    {
        return slider.value;
    }
}
