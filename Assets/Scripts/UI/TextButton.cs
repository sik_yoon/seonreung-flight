﻿using UnityEngine;
using UnityEngine.UI;

public class TextButton : MonoBehaviour
{
    public Button button;
    public Text text;

    public void SetInteractable(bool flag)
    {
        button.interactable = flag;
    }

    public void SetText(string str, int size)
    {
        text.fontSize = size;
        text.text = str;
    }

    public void SetText(string str)
    {
        text.text = str;
    }
}
