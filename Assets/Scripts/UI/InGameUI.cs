﻿using UnityEngine;
using UnityEngine.Advertisements;
using System.Collections;
using UnityEngine.UI;

public class InGameUI : MonoBehaviour
{
    public GameObject gameOverUI;

    void OnEnable()
    {
        gameOverUI.SetActive(false);
    }

    public void SetGameOverUIActive(bool flag)
    {
        gameOverUI.SetActive(flag);
    }

    public void ToggleTimeScale()
    {
        if(PlayerCtrl.instance.IsAlive())
        {
            if(Time.timeScale == 1)
            {
                Time.timeScale = 0;
                UIManager.instance.moveSlider.SetInteractable(false);
            }

            else
            {
                Time.timeScale = 1;
                UIManager.instance.moveSlider.SetInteractable(true);
            }
        }
    }

    public void ViewAd()
    {
        Debug.Log("view ad");
        if (Advertisement.IsReady())
        {
            Advertisement.Show();
        }
    }

    public void GoToLobby()
    {
        GameManager.instance.GoToLobby();
    }
}
