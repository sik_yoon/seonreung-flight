﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class CharacterListItem : MonoBehaviour
{
    public static Color offColor = new Color32(191, 191, 191, 255);
    public static Color onColor = new Color32(138, 186, 106, 255);
    public static Color usingColor = new Color32(186, 138, 106, 255);

    public Text levelNumberText;

    public TextButton buyButton;
    public TextButton useButton;
    public TextButton upgradeButton;
    public GameObject previewButton;

    public StatusText status1Text;
    public StatusText status2Text;
    public StatusText status3Text;

    protected Weapon weapon;

    protected virtual void Init()
    {
        buyButton.gameObject.SetActive(true);
        previewButton.SetActive(true);
        useButton.gameObject.SetActive(false);
        upgradeButton.gameObject.SetActive(false);

        weapon = PlayerCtrl.instance.GetWeapon(name);

        if(weapon.GetUpgradeLevel() != 0)
        {
            ChangeUIToSoldOut();

            if(GameManager.instance.GetSelectedCharacterName() == name)
            {
                useButton.SetInteractable(false);
                useButton.SetText("USING");

                UIManager.instance.selectCharacterUI.currentItem = this;
            }
        }
        else
            buyButton.SetText("BUY\n" + weapon.GetCurrentUpgradeCost() + "[G]");

        levelNumberText.text = "LV." + weapon.GetUpgradeLevel() + "/" + weapon.GetMaxUpgradeLevel();

        SetStatus();
    }

    private void ChangeUIToSoldOut()
    {
        buyButton.gameObject.SetActive(false);
        previewButton.SetActive(false);
        useButton.gameObject.SetActive(true);
        upgradeButton.gameObject.SetActive(true);

        SetUpgradeInfo();
    }

    private void SetUpgradeInfo()
    {
        levelNumberText.text = "LV." + weapon.GetUpgradeLevel() + "/" + weapon.GetMaxUpgradeLevel();

        if(weapon.GetUpgradeLevel() == weapon.GetMaxUpgradeLevel())
        {
            upgradeButton.SetText("UPGRADE COMPLETE", 30);
        }
        else upgradeButton.SetText("UPGRADE\n" + weapon.GetCurrentUpgradeCost() + "[G]");

        SetStatus();
    }

    protected abstract void SetStatus();

    public void BuyCharacter()
    {
        if(GameManager.instance.GetGoldAmount() < weapon.GetCurrentUpgradeCost()) return;

        GameManager.instance.SetGoldAmount(GameManager.instance.GetGoldAmount() - weapon.GetCurrentUpgradeCost());
        weapon.Upgrade();
        ChangeUIToSoldOut();

        UseCharacter();

        UIManager.instance.selectCharacterUI.CheckItems();
    }

    public void UpgradeCharacter()
    {
        if(GameManager.instance.GetGoldAmount() < weapon.GetCurrentUpgradeCost()) return;

        GameManager.instance.SetGoldAmount(GameManager.instance.GetGoldAmount() - weapon.GetCurrentUpgradeCost());
        weapon.Upgrade();
        SetUpgradeInfo();
        PlayerCtrl.instance.StopShoot();
        PlayerCtrl.instance.StartShoot();

        UIManager.instance.selectCharacterUI.CheckItems();
    }

    public void UseCharacter()
    {
        if(UIManager.instance.selectCharacterUI.currentItem == this) return;

        UIManager.instance.selectCharacterUI.currentItem.useButton.SetInteractable(true);
        UIManager.instance.selectCharacterUI.currentItem.useButton.SetText("USE");
        useButton.SetInteractable(false);
        useButton.SetText("USING");

        PlayerCtrl.instance.StopShoot();
        PlayerCtrl.instance.FocusCharacter(name);
        PlayerCtrl.instance.StartShoot();

        GameManager.instance.SetSelectedCharacterName(name);

        UIManager.instance.selectCharacterUI.currentItem = this;
    }

    public void PreviewCharacter()
    {
        PlayerCtrl.instance.StopShoot();
        PlayerCtrl.instance.FocusCharacter(name);
        PlayerCtrl.instance.StartPreviewShoot();
    }

    public void CheckButtons()
    {
        int goldAmount = GameManager.instance.GetGoldAmount();

        if(weapon.GetUpgradeLevel() == 0)
        {
            if(goldAmount < weapon.GetCurrentUpgradeCost())
                buyButton.SetInteractable(false);
            else buyButton.SetInteractable(true);
        }
        else
        {
            if(goldAmount < weapon.GetCurrentUpgradeCost() || weapon.GetUpgradeLevel() == weapon.GetMaxUpgradeLevel())
                upgradeButton.SetInteractable(false);
            else upgradeButton.SetInteractable(true);
        }
    }
}
