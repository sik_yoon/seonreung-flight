﻿using UnityEngine;
using System.Collections;

public class BulletWeaponCharacterListItem : CharacterListItem
{
    private BulletWeapon bulletWeapon;

    protected override void SetStatus()
    {
        if(bulletWeapon == null) bulletWeapon = (BulletWeapon)weapon;

        int upgradeLevel = bulletWeapon.GetUpgradeLevel();
        int maxUpgradeLevel = bulletWeapon.GetMaxUpgradeLevel();

        int currentDamage, nextDamage;
        float currentReloadTime, nextReloadTime;
        int currentBulletNum, nextBulletNum;

        if(upgradeLevel == 0)
        {
            currentDamage = 0;
            currentReloadTime = 100;
            currentBulletNum = 0;
        }
        else
        {
            currentDamage = bulletWeapon.GetCurrentDamage();
            currentReloadTime = bulletWeapon.GetCurrentReloadTime();
            currentBulletNum = bulletWeapon.GetCurrentBulletNum();
        }

        if(upgradeLevel == maxUpgradeLevel)
        {
            nextDamage = 0;
            nextReloadTime = 100;
            nextBulletNum = 0;
        }
        else
        {
            nextDamage = bulletWeapon.GetNextDamage();
            nextReloadTime = bulletWeapon.GetNextReloadTime();
            nextBulletNum = bulletWeapon.GetNextBulletNum();
        }

        status1Text.SetText(currentDamage, nextDamage);
        status2Text.SetText(currentReloadTime, nextReloadTime);
        status3Text.SetText(currentBulletNum, nextBulletNum);
    }

    void Awake()
    {
        Init();
    }

    void OnEnable()
    {
        CheckButtons();
    }
}
