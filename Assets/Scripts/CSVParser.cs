﻿using UnityEngine;
using System.Collections.Generic;

public class CSVParser : MonoBehaviour
{
    public static CSVParser instance;

    public string path = "DataSheets/";

    [HideInInspector]
    public StageInfo[] stageInfo;
    public string spawnDataFileName = "SpawnData";

    [HideInInspector]
    public Dictionary<string, EnemyInfo> enemyInfo;
    public string enemyDataFileName = "EnemyData";

    void Awake()
    {
        instance = this;

        stageInfo = ParseSpawnData(path + spawnDataFileName);
        enemyInfo = ParseEnemyData(path + enemyDataFileName);
    }

    public string ReadCSVToString(string fileName)
    {
        return (Resources.Load(fileName) as TextAsset).text;
    }

    private StageInfo[] ParseSpawnData(string fileName)
    {
        StageInfo[] stageInfo = null;

        string[] fileData = ReadCSVToString(fileName).Split('\n');

        int lineNum = 0;

        // stage number
        string[] lineData = fileData[lineNum++].Split(',');
        int stageNum = int.Parse(lineData[0]);

        stageInfo = new StageInfo[stageNum + 1];
        for( ; lineNum < fileData.Length ; ++lineNum)
        {
            // stage, status ratio, zombie number, zombie spawn interval, enemy number
            lineData = fileData[lineNum++].Split(',');
            int stage = int.Parse(lineData[0]);

            stageInfo[stage] = new StageInfo(
                float.Parse(lineData[1]),
                int.Parse(lineData[2]),
                float.Parse(lineData[3]),
                int.Parse(lineData[4])
            );

            for(int enemy = 0 ; enemy < stageInfo[stage].spawnInfo.Length ; enemy++)
            {
                // name, xpos, zpos, angle, time
                lineData = fileData[lineNum++].Split(',');
                stageInfo[stage].spawnInfo[enemy] = new SpawnInfo(
                    lineData[0],
                    float.Parse(lineData[1]),
                    float.Parse(lineData[2]),
                    int.Parse(lineData[3]),
                    float.Parse(lineData[4])
                );
            }
        }

        return stageInfo;
    }

    private Dictionary<string, EnemyInfo> ParseEnemyData(string fileName)
    {
        Dictionary<string, EnemyInfo> enemyInfo = new Dictionary<string, EnemyInfo>();

        string[] fileData = ReadCSVToString(fileName).Split('\n');

        for(int lineNum = 0 ; lineNum < fileData.Length ; )
        {
            // enemy name, hp, move speed, model number
            string[] lineData = fileData[lineNum++].Split(',');

            string enemyName = lineData[0];
            if(enemyName == "") continue;

            enemyInfo[enemyName] = new EnemyInfo(
                float.Parse(lineData[1]),
                float.Parse(lineData[2]),
                float.Parse(lineData[3]),
                int.Parse(lineData[4])
            );

            // model names
            lineData = fileData[lineNum++].Split(',');
            string[] modelNames = enemyInfo[enemyName].modelNames;
            for(int model = 0 ; model < modelNames.Length ; ++model)
            {
                modelNames[model] = lineData[model];
            }
        }

        return enemyInfo;
    }
}
