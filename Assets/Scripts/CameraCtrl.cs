﻿using UnityEngine;
using System.Collections;

public class CameraCtrl : MonoBehaviour
{
    public static CameraCtrl instance;

    private Transform tr;

    private Vector3 originalPos;
    private Quaternion originalRot;

    void Awake()
    {
        instance = this;

        tr = GetComponent<Transform>();
        originalPos = tr.position;
        originalRot = tr.rotation;
    }

    public void RandomShake()
    {
        StartCoroutine(RandomShakeRoutine());
    }

    IEnumerator RandomShakeRoutine()
    {
        for(float t = 0 ; t < 0.15f ; t += Time.deltaTime)
        {
            tr.position += Random.insideUnitSphere * Time.timeScale * 0.1f;

            yield return null;
        }

        tr.position = originalPos;
        tr.rotation = originalRot;
    }

    public void ChangeCameraToSelectCharacter()
    {
        gameObject.MoveTo(new Vector3(-9f, 17, -8), 1f, 0);
        gameObject.RotateTo(new Vector3(50, -13, 0), 1f, 0);
    }

    public void ChangeCameraToLobby()
    {
        gameObject.MoveTo(originalPos, 1f, 0);
        gameObject.RotateTo(originalRot.eulerAngles, 1f, 0);
    }
}
