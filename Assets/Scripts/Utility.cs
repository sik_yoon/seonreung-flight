﻿using UnityEngine;
using System.Collections;

public static class Utility
{
    public static Color32 HexToColor32(string hex)
    {
        if(hex == "") hex = "FFFFFFFF";

        int hexColor = int.Parse(hex, System.Globalization.NumberStyles.HexNumber);
        byte a = (byte)(hexColor & 0xFF);
        byte b = (byte)((hexColor >>= 8) & 0xFF);
        byte g = (byte)((hexColor >>= 8) & 0xFF);
        byte r = (byte)((hexColor >>= 8) & 0xFF);

        return new Color32(r, g, b, a);
    }
}
