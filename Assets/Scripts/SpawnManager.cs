﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnManager : MonoBehaviour
{
    public static SpawnManager instance;

    private StageInfo[] stageInfo;
    private Dictionary<string, EnemyInfo> enemyInfo;

    private float ypos = 0f;

    private int stage;
    public int enemiesLeft;

    void Awake()
    {
        instance = this;

        stageInfo = CSVParser.instance.stageInfo;
        enemyInfo = CSVParser.instance.enemyInfo;
    }

    public void DiscountEnemiesLeft()
    {
        enemiesLeft--;
        if(enemiesLeft == 0) MoveNextStage();
    }

    IEnumerator EnemySpawnRoutine()
    {
        yield return new WaitForSeconds(1.0f);

        SpawnInfo[] spawnInfo = stageInfo[stage].spawnInfo;
        float statusRatio = stageInfo[stage].statusRatio;

        SpawnInfo si;
        EnemyInfo ei;
        float time = 0;
        for(int i = 0 ; PlayerCtrl.instance.IsAlive() && i < spawnInfo.Length ; i++)
        {
            si = spawnInfo[i];
            ei = enemyInfo[si.name];

            if(si.time != time)
            {
                yield return new WaitForSeconds(si.time - time);
                time = si.time;
            }

            EnemyCtrl enemy = ObjectPoolManager.instance.GetPooledObject(
                ei.modelNames[Random.Range(0, ei.modelNames.Length)],
                new Vector3(si.xpos, ypos, si.zpos),
                Quaternion.Euler(0, 180 + si.angle, 0)
            ).GetComponent<EnemyCtrl>();
            enemy.SetStatus((int)(ei.hp * statusRatio), ei.moveSpeed * statusRatio, ei.scale);
            enemy.gameObject.SetActive(true);
        }
    }

    IEnumerator ZombieSpawnRoutine()
    {
        float statusRatio = stageInfo[stage].statusRatio;
        float moveSpeedRatio = 1 + (statusRatio - 1) * 0.2f;
        float maxXPos = PlayerCtrl.instance.maxXPos;

        EnemyInfo ei = enemyInfo["Z-Normal"];

        for(int i = 0 ; PlayerCtrl.instance.IsAlive() && i < stageInfo[stage].zombieNum ; i++)
        {
            ZombieCtrl zombie = ObjectPoolManager.instance.GetPooledObject(
                ei.modelNames[Random.Range(0, ei.modelNames.Length)],
                new Vector3(Random.Range(-maxXPos, maxXPos), ypos, 40f),
                Quaternion.Euler(0, 180, 0)
            ).GetComponent<ZombieCtrl>();

            zombie.SetStatus(
                (int)(ei.hp * statusRatio),
                Random.Range(ei.moveSpeed * moveSpeedRatio * 1f, ei.moveSpeed * moveSpeedRatio * 5f),
                Random.Range(0.8f, 1.2f) * ei.scale
            );
            zombie.gameObject.SetActive(true);

            float interval = stageInfo[stage].zombieSpawnInterval;
            yield return new WaitForSeconds(Random.Range(interval * 0.5f, interval * 1.5f));
        }
    }

    public void StartSpawn(int stage)
    {
        // this.stage = stage / 10 * 10;
        this.stage = stage;
        MoveNextStage();
    }

    private void MoveNextStage()
    {
        if(PlayerCtrl.instance.IsAlive())
        {
            GameManager.instance.SetClearedStage(stage);

            if(stage < 30) stage++;

            enemiesLeft = stageInfo[stage].zombieNum + stageInfo[stage].enemyNum;

            StartCoroutine(ZombieSpawnRoutine());
            StartCoroutine(EnemySpawnRoutine());
        }
    }
}
