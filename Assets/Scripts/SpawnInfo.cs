﻿
public struct SpawnInfo
{
    public string name;
    public float xpos;
    public float zpos;
    public int angle;
    public float time;

    public SpawnInfo(string name, float xpos, float zpos, int angle, float time)
    {
        this.name = name;
        this.xpos = xpos;
        this.zpos = zpos;
        this.angle = angle;
        this.time = time;
    }
}
