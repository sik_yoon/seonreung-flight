﻿using UnityEngine;
using System.Collections;

public class Test : MonoBehaviour
{
    void Start()
    {
        gameObject.MoveFrom(new Vector3(0, 0, 20), 10, 0, EaseType.linear);
        StartCoroutine(TestRoutine());
    }

    IEnumerator TestRoutine()
    {
        yield return new WaitForSeconds(3f);
        iTween.Pause(gameObject);
        yield return new WaitForSeconds(3f);
        iTween.Resume(gameObject);
    }
}
