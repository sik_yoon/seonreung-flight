﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;

    private AudioSource[] dieSounds;
    private AudioSource[] growlSounds;
    private AudioSource[] pigeonSounds;

    private int enemyDie;

    void Awake()
    {
        instance = this;

        dieSounds = transform.Find("Die").GetComponents<AudioSource>();
        growlSounds = transform.Find("Growl").GetComponents<AudioSource>();
        pigeonSounds = transform.Find("Pigeon").GetComponents<AudioSource>();
    }

    void Start()
    {
        StartCoroutine(DieSoundRoutine());
        StartCoroutine(GrowlSoundRoutine());
        StartCoroutine(PigeonSoundRoutine());
    }

    public void PlayEnemyDieSound()
    {
        enemyDie++;
    }

    IEnumerator DieSoundRoutine()
    {
        AudioSource dieSound;
        while(true)
        {
            if(enemyDie > 0)
            {
                enemyDie = enemyDie < 3 ? enemyDie : 3;
                for(int i = 0 ; i < enemyDie ; i++)
                {
                    dieSound = dieSounds[Random.Range(0, dieSounds.Length)];
                    dieSound.pitch = Random.Range(0.8f, 1.0f);
                    dieSound.Play();
                }

                enemyDie = 0;
            }

            yield return null;
        }
    }

    IEnumerator GrowlSoundRoutine()
    {
        AudioSource growlSound;
        while(true)
        {
            yield return new WaitForSeconds(Random.Range(5f, 10f));

            growlSound = growlSounds[Random.Range(0, growlSounds.Length)];
            growlSound.pitch = Random.Range(0.7f, 1.0f);
            growlSound.Play();
        }
    }

    IEnumerator PigeonSoundRoutine()
    {
        AudioSource pigeonSound;
        while(true)
        {
            pigeonSound = pigeonSounds[Random.Range(0, pigeonSounds.Length)];
            pigeonSound.pitch = Random.Range(0.55f, 0.8f);
            pigeonSound.Play();

            yield return new WaitForSeconds(Random.Range(10f, 20f));
        }
    }
}
