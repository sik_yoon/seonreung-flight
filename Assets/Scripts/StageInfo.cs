﻿
public struct StageInfo
{
    public float statusRatio;
    public int zombieNum;
    public float zombieSpawnInterval;
    public int enemyNum;
    public SpawnInfo[] spawnInfo;

    public StageInfo(float statusRatio, int zombieNum, float zombieSpawnInterval, int enemyNum)
    {
        this.statusRatio = statusRatio;
        this.zombieNum = zombieNum;
        this.zombieSpawnInterval = zombieSpawnInterval;
        this.enemyNum = enemyNum;
        this.spawnInfo = new SpawnInfo[enemyNum];
    }
}