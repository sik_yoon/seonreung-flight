﻿using UnityEngine;
using System.Collections;

public class ParticleCtrl : MonoBehaviour
{
    public ParticleSystem particle;

    void OnEnable()
    {
        particle.startSize = Random.Range(0, 30);

        StartCoroutine(DestroyRoutine());
    }

    IEnumerator DestroyRoutine()
    {
        yield return new WaitForSeconds(1.0f);
        ObjectPoolManager.instance.ReturnPooledObject(gameObject);
    }
}
