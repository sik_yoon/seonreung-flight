﻿using System;
using System.Text;
using System.Security.Cryptography;

public class AesCrypt
{
    UTF8Encoding enc;
    RijndaelManaged rcipher;
    byte[] key, pwd, ivBytes, iv;

    private enum EncryptMode { ENCRYPT, DECRYPT };

    static readonly char[] CharacterMatrixForRandomIVStringGeneration =
    {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_'
    };

    public AesCrypt()
    {
        enc = new UTF8Encoding();
        rcipher = new RijndaelManaged();
        //rcipher = new AesManaged();
        rcipher.Mode = CipherMode.CBC;
        rcipher.Padding = PaddingMode.PKCS7;
        //rcipher.Padding = PaddingMode.None;
        rcipher.KeySize = 256;
        rcipher.BlockSize = 128;
        key = new byte[32];
        iv = new byte[rcipher.BlockSize / 8];
        ivBytes = new byte[16];
    }

    internal static string GenerateRandomIV(int length)
    {
        char[] iv = new char[length];
        byte[] randomBytes = new byte[length];

        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
        rng.GetBytes(randomBytes);
        for (int i = 0; i < iv.Length; i++)
        {
            int ptr = randomBytes[i] % CharacterMatrixForRandomIVStringGeneration.Length;
            iv[i] = CharacterMatrixForRandomIVStringGeneration[ptr];
        }
        return new string(iv);
    }

    private String encryptDecrypt(string input, string encryptionKey, EncryptMode mode, string initVector)
    {
        string outPut = "";
        // encryptionKey = MD5Hash (_encryptionKey);
        pwd = Encoding.UTF8.GetBytes(encryptionKey);
        ivBytes = Encoding.UTF8.GetBytes(initVector);
        int len = pwd.Length;

        if (len > key.Length)
        {
            len = key.Length;
        }
        int ivLength = ivBytes.Length;
        if (ivLength > iv.Length)
        {
            ivLength = iv.Length;
        }

        Array.Copy(pwd, key, len);
        Array.Copy(ivBytes, iv, ivLength);
        rcipher.Key = key;
        rcipher.IV = iv;

        if (mode.Equals(EncryptMode.ENCRYPT))
        {
            byte[] plainText = rcipher.CreateEncryptor().TransformFinalBlock(enc.GetBytes(input), 0, input.Length);
            outPut = Convert.ToBase64String(plainText);
        }
        else if (mode.Equals(EncryptMode.DECRYPT))
        {
            byte[] plainText = rcipher.CreateDecryptor().TransformFinalBlock(Convert.FromBase64String(input), 0, Convert.FromBase64String(input).Length);
            outPut = enc.GetString(plainText);
        }

        rcipher.Clear();
        return outPut;
    }

    public string encrypt(string plainText, string key, string initVector)
    {
        return encryptDecrypt(plainText, key, EncryptMode.ENCRYPT, initVector);
    }

    public string decrypt(string encryptedText, string key, string initVector)
    {
        return encryptDecrypt(encryptedText, key, EncryptMode.DECRYPT, initVector);
    }

    public static string getHashSha256(string text, int length)
    {
        byte[] bytes = Encoding.UTF8.GetBytes(text);
        SHA256Managed hashstring = new SHA256Managed();
        byte[] hash = hashstring.ComputeHash(bytes);
        string hashString = string.Empty;
        foreach (byte x in hash)
        {
            hashString += String.Format("{0:x2}", x); //covert to hex string
        }
        if (length > hashString.Length)
            return hashString;
        else
            return hashString.Substring(0, length);
    }

    private static string MD5Hash(string text)
    {
        MD5 md5 = new MD5CryptoServiceProvider();

        md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

        byte[] result = md5.Hash;

        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < result.Length; i++)
        {
            strBuilder.Append(result[i].ToString("x2"));
        }
        Console.WriteLine("md5 hash of they key=" + strBuilder.ToString());
        return strBuilder.ToString();
    }
}
