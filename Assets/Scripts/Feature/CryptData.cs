﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Security;
using System.Security.Cryptography;
using System;


public class CryptData : MonoBehaviour
{    
    string uri = "https://www.swdev.kr/crypt";

    // Use this for initialization
    void Start()
    {
        //Cipher();
        Aes();
    }

    private JSONObject CreateJSON()
    {
        JSONObject obj = new JSONObject(JSONObject.Type.OBJECT);
        obj.AddField("user_id", "hochul@naver.com");
        obj.AddField("country", "korea");
        obj.AddField("play_time", 5.0f);
        return obj;
    }

    private IEnumerator ServerInit(string uri, string data)
    {
        Dictionary<string, string> head = new Dictionary<string, string>();
        head.Add("Content-Type", "text/plain");
        //head.Add("Content-Type", "application/json");
        head.Add("Content-Length", data.Length.ToString());

        WWW w = new WWW(uri, Encoding.UTF8.GetBytes(data), head);
        yield return w;
    }

    private void Aes()
    {
        string beforeCipher = "1234567890123456";
        beforeCipher = CreateJSON().Print();
        string aesKey = "12345678901234567890123456789012";
        AesManaged aesManaged = new AesManaged();
        aesManaged.Mode = CipherMode.CBC;

        try
        {
            aesManaged.KeySize = 256;
            aesManaged.Key = System.Text.Encoding.UTF8.GetBytes("12345678901234567890123456789012");
            aesManaged.IV = System.Text.Encoding.UTF8.GetBytes("1234567890123456");
            Debug.Log(aesKey.Length);
            Debug.Log("Input string : " + beforeCipher);

            ICryptoTransform transform = aesManaged.CreateEncryptor();
            byte[] plainText = System.Text.Encoding.UTF8.GetBytes(beforeCipher);
            byte[] encrypted = transform.TransformFinalBlock(plainText, 0, plainText.Length);

            string base64ed = Convert.ToBase64String(encrypted);

            Debug.Log("Cipher string : " + base64ed);

            transform = aesManaged.CreateDecryptor();
            encrypted = Convert.FromBase64String(base64ed);
            plainText = transform.TransformFinalBlock(encrypted, 0, encrypted.Length);

            string decrypted = System.Text.Encoding.UTF8.GetString(plainText);

            Debug.Log("Decipher string : " + decrypted);

            JSONObject o = new JSONObject(JSONObject.Type.OBJECT);
            o.AddField("key", aesKey);
            o.AddField("data", base64ed);
            o.AddField("aesKey", aesKey);
            StartCoroutine(ServerInit(uri, o.Print()));
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
        }
    }

    private void Cipher()
    {
        AesCrypt crypt = new AesCrypt();
        //string plainText = "This is the text to be encrypted";
        string plainText = CreateJSON().Print();

        string iv = AesCrypt.GenerateRandomIV(16); //16 bytes = 128 bits
        string aesKey = AesCrypt.getHashSha256("my secret key", 31); //32 bytes = 256 bits
        string cypherText = crypt.encrypt(plainText, aesKey, iv);

        //iv = "1234567890123456";

        // 암호화 개체 생성
        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
        // 개인키 생성
        RSAParameters privateKey = RSA.Create().ExportParameters(true);
        rsa.ImportParameters(privateKey);
        string privateKeyText = rsa.ToXmlString(true);
        // 공개키 생성
        RSAParameters publicKey = new RSAParameters();
        publicKey.Modulus = privateKey.Modulus;
        publicKey.Exponent = privateKey.Exponent;
        rsa.ImportParameters(publicKey);
        string publicKeyText = rsa.ToXmlString(false);

        //privateKeyText= "<RSAKeyValue><Modulus>2aSeqboynDA9ppdlBh5+CythRjJQYmMyBi1Y1F8OQPieYk4KsWrupUOdAmRRY36yJnetJvBpHe4C0iwbTMJ55HSSWcH9lfawe43KNOI3wirUuqD8KuKOMqhc3G6C/+7vVC59yZz2H4KUo0EIEnzv6dEtQMcv80AefJY2Bda48A0=</Modulus><Exponent>EQ==</Exponent><P>+3GxGF7KsZT7kfkFcqOdkBfHUqiuG2ytY8z+CJxCv8KADJwZ668awvUPyLdGfmPLq2k8SZ/4uby1wd75Tr3akQ==</P><Q>3ZYk0aplYi7xPCGNSXC/ZT4AajwoPkXaTkCdnLNk4BMWl5BlLzHIa0CP4wwEjc//mdkfLpO7RvFJXxxq/8DjvQ==</Q><DP>wEfhx1eL8TWxUYIxV6pLUBIvAvl2FPjA09j+fw4U7P4lr0ox8HbYOrtmbE/qnOLnCpvEsMWgFZBOwW5GLSfFQQ==</DP><DQ>tnul2deeyVPVuQySeLc0NUIedZrz9wxZbZ6f6nWeXi3WXrMmJt20HBcNFVUw7UHhjcHdcabWdqiWxswb4a3ouQ==</DQ><InverseQ>AAYicZV6ivG0HEnDxYwhh3oFpkNEW+3VmInkL2vdpFrLaAO1BNZSjRQ2yDTXm2BlbIqQnU80Pet1I+khXdKU3A==</InverseQ><D>QAM9uXL/05W3x5XwiVRDMHYrqzv5hllpEOApL2dAbXZMs4Bdf3nNuCLx8aUI4QclVpurZc49CM2IW+7a2ldRBmn7kDB1DuhjsYOsIKHOLNN/sNmsgkZeuc/5MSweKQg0v0bMnXGNxnMrAQ3I711kgXRyQ3GVfAOZjk+n1SgRHbE=</D></RSAKeyValue>";
        //publicKeyText = "<RSAKeyValue><Modulus>2aSeqboynDA9ppdlBh5+CythRjJQYmMyBi1Y1F8OQPieYk4KsWrupUOdAmRRY36yJnetJvBpHe4C0iwbTMJ55HSSWcH9lfawe43KNOI3wirUuqD8KuKOMqhc3G6C/+7vVC59yZz2H4KUo0EIEnzv6dEtQMcv80AefJY2Bda48A0=</Modulus><Exponent>EQ==</Exponent></RSAKeyValue>";
        //iv = "Ph_tP2UaCISe1Zt_";
        //aesKey = "b16920894899c7780b5fc7161560a41";

        string keyEncrypted = RSACrypt.RSAEncrypt(aesKey, publicKeyText);
        string keyDecrypted = RSACrypt.RSADecrypt(RSACrypt.RSAEncrypt(aesKey, publicKeyText), privateKeyText);
        Debug.Log("Private key : " + privateKeyText);
        Debug.Log("Public key : " + publicKeyText);
        Debug.Log("Aes Encrypted by RSA: " + keyEncrypted);
        Debug.Log("Aes Decrypted by RSA: " + keyDecrypted);

        JSONObject o = new JSONObject(JSONObject.Type.OBJECT);
        o.AddField("key", keyEncrypted);
        o.AddField("data", cypherText);
        o.AddField("aesKey", aesKey);
        StartCoroutine(ServerInit(uri, o.Print()));

        Debug.Log("iv=" + iv);
        Debug.Log("Aes key=" + aesKey);
        Debug.Log("Cypher text=" + cypherText);
        Debug.Log("Plain text =" + crypt.decrypt(cypherText, keyDecrypted, iv));
    }
}
