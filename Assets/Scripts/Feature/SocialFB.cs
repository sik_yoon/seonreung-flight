﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;

namespace Facebook.Unity.Example
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    internal sealed class SocialFB : MenuBase
    {
        public GameObject loginButton;
        public GameObject loginPanel;

        protected override void Awake()
        {
            base.Awake();
            FB.Init(this.OnInitComplete, this.OnHideUnity);                        

            if(PlayerPrefs.GetInt("FBlogin") == 1)
            {
                loginButton.SetActive(false);
            }
        }

        protected override bool ShowBackButton()
        {
            return false;
        }

        protected void OnGUI() { }

        protected override void GetGui()
        {
            bool enabled = GUI.enabled;

            if (this.Button("FB.Init"))
            {
                FB.Init(this.OnInitComplete, this.OnHideUnity);
                this.Status = "FB.Init() called with " + FB.AppId;
            }

            GUILayout.BeginHorizontal();

            GUI.enabled = enabled && FB.IsInitialized;
            if (this.Button("Login"))
            {
                this.CallFBLogin();
                this.Status = "Login called";
            }
        }

        private void CallFBLogin()
        {
            loginPanel.SetActive(true);
            FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends" }, AuthCallback);
            Screen.orientation = ScreenOrientation.Portrait;            
        }

        private void CallFBLoginForPublish()
        {
            // It is generally good behavior to split asking for read and publish
            // permissions rather than ask for them all at once.
            //
            // In your own game, consider postponing this call until the moment
            // you actually need it.
            FB.LogInWithPublishPermissions(new List<string>() { "publish_actions" }, this.HandleResult);
        }

        private void CallFBLogout()
        {
            FB.LogOut();
        }

        private void OnInitComplete()
        {
            this.Status = "Success - Check log for details";
            this.LastResponse = "Success Response: OnInitComplete Called\n";
            string logMessage = string.Format(
                "OnInitCompleteCalled IsLoggedIn='{0}' IsInitialized='{1}'",
                FB.IsLoggedIn,
                FB.IsInitialized);
            LogView.AddLog(logMessage);
            if (AccessToken.CurrentAccessToken != null)
            {
                LogView.AddLog(AccessToken.CurrentAccessToken.ToString());
            }
        }

        private void OnHideUnity(bool isGameShown)
        {
            this.Status = "Success - Check log for details";
            this.LastResponse = string.Format("Success Response: OnHideUnity Called {0}\n", isGameShown);
            LogView.AddLog("Is game shown: " + isGameShown);            
        }

        private void AuthCallback(ILoginResult result)
        {            
            if (FB.IsLoggedIn)
            {
                // AccessToken class will have session details
                var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
                // Print current access token's User ID
                Debug.Log(aToken.UserId);
                // Print current access token's granted permissions
                foreach (string perm in aToken.Permissions)
                {
                    Debug.Log(perm);
                }

                loginButton.SetActive(false);
                FetchFBProfile();                
            }
            else {
                Debug.Log("User cancelled login");
                
            }
            loginPanel.SetActive(false);
            Screen.orientation = ScreenOrientation.LandscapeLeft;
        }

        public void FBLogin()
        {            
            Debug.Log("buuton");
            this.CallFBLogin();
            this.Status = "Login called";
        }

        private void FetchFBProfile()
        {
            FB.API("/me?fields=id,name,email,gender", HttpMethod.GET, FetchProfileCallback, new Dictionary<string, string>() { });
        }

        private void FetchProfileCallback(IGraphResult result)
        {
            Debug.Log(result.RawResult);

            Dictionary<string, object> FBUserDetails = (Dictionary<string, object>)result.ResultDictionary;
            Debug.Log(FBUserDetails["email"]);
            Debug.Log(FBUserDetails["id"]);
            Debug.Log(FBUserDetails["name"]);

            PlayerPrefs.SetInt("FBlogin", 1);

            PlayerPrefs.SetString("email", FBUserDetails["email"].ToString());
            PlayerPrefs.SetString("id", FBUserDetails["id"].ToString());
            PlayerPrefs.SetString("name", FBUserDetails["name"].ToString());
            PlayerPrefs.SetString("gender", FBUserDetails["gender"].ToString());
            PlayerPrefs.Save();            
        }        
    }
}
