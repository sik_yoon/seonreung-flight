﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;
using UnityEngine.UI;

public class Ranking : MonoBehaviour
{
    string uri = "https://www.swdev.kr/showrank";
    //string uri = "https://www.swdev.kr/setrank";

    public GameObject rank;
    public GameObject rankList;
    public GameObject rankUI;
    private GameObject rankObj;
    private bool isRankActive = true;


    void Awake()
    {
        StartCoroutine(Get(uri));
        //StartCoroutine(Post(uri));
    }    

    IEnumerator Post(string uri)
    {
        JSONObject obj = new JSONObject(JSONObject.Type.OBJECT);
        System.Random r = new System.Random((int)DateTime.Now.Ticks);
        int score = r.Next(0, 100);
        obj.AddField("user", "user_" + r.Next(0, 500) + "@naver.com");
        obj.AddField("score", score);
        obj.AddField("play_time", 5.0f);
        Debug.Log(score);
        string data = obj.Print();

        Dictionary<string, string> head = new Dictionary<string, string>();
        head.Add("Content-Type", "text/plain");
        head.Add("Content-Length", data.Length.ToString());
        JSONObject o = new JSONObject(data);

        WWW w = new WWW(uri, Encoding.UTF8.GetBytes(data), head); // POST
        //w = new WWW(uri); // GET
        yield return w;
    }

    IEnumerator Get(string url)
    {
        WWW www = new WWW(url);
        StartCoroutine(WaitForRequest(www));
        yield return www;
    }

    private IEnumerator WaitForRequest(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            ShowRanking(www.text);
        }
        else
        {
            Debug.Log("WWW error : " + www.error);
        }
    }

    private void ShowRanking(string data)
    {
        Debug.Log("WWW Ok! : " + data);
        string[] rank = data.Split(',');
        rank[0] = rank[0].Substring(1);
        int last = rank.Length - 1;
        rank[last] = rank[last].Substring(0, rank[last].Length - 1);

        for (int i = 0; i < rank.Length; i++)
        {
            rank[i] = rank[i].Substring(1, rank[i].Length - 2);
        }

        foreach (string a in rank)
        {
            Debug.Log(a);
        }

        int j = 1;

        for (int i = 0; i < rank.Length; i += 2)
        {
            GameObject rankObj = Instantiate(this.rank);            
            rankObj.transform.SetParent(rankList.transform, false);            
            Text txtRank = rankObj.transform.FindChild("RankText").GetComponent<Text>();
            txtRank.text = "" + j++;
            Text txtName = rankObj.transform.FindChild("NameText").GetComponent<Text>();
            txtName.text = rank[i];
            Text txt = rankObj.transform.FindChild("ScoreText").GetComponent<Text>();
            txt.text = rank[i + 1];
        }        
    }

    public void ActiveRank()
    {
        rankUI.SetActive(isRankActive);
        isRankActive = !isRankActive;
    }


}
