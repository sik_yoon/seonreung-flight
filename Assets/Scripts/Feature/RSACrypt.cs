﻿using UnityEngine;
using System.Collections;
using System;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.IO;

public class RSACrypt : MonoBehaviour {

	// Use this for initialization
	void Start () {
        //RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

        //RSAParameters privateKey = RSA.Create().ExportParameters(true);
        //rsa.ImportParameters(privateKey);
        //string privateKeyText = rsa.ToXmlString(true);

        //RSAParameters publicKey = new RSAParameters();
        //publicKey.Modulus = privateKey.Modulus;
        //publicKey.Exponent = privateKey.Exponent;
        //rsa.ImportParameters(publicKey);
        //string publicKeyText = rsa.ToXmlString(false);

        //Debug.Log("RSA Encrypt : " + RSAEncrypt("This is the test for RSA.", publicKeyText));
        //string decryptStr = RSAEncrypt("This is the test for RSA.", publicKeyText);
        //Debug.Log("RSA Decrypt : " + RSADecrypt(decryptStr, privateKeyText));
    }

    // Update is called once per frame
    void Update () {
	
	}

    public static string RSAEncrypt(string sValue, string sPubKey)
    {
        //공개키 생성        
        System.Security.Cryptography.RSACryptoServiceProvider oEnc = new RSACryptoServiceProvider(); //암호화

        oEnc.FromXmlString(sPubKey);

        //암호화할 문자열을 UFT8인코딩
        byte[] inbuf = (new UTF8Encoding()).GetBytes(sValue);
        //암호화
        byte[] encbuf = oEnc.Encrypt(inbuf, false);

        //암호화된 문자열 Base64인코딩
        return Convert.ToBase64String(encbuf);
    }

    public static string RSADecrypt(string sValue, string sPrvKey)
    {
        //RSA객체생성
        System.Security.Cryptography.RSACryptoServiceProvider oDec = new RSACryptoServiceProvider(); //복호화
                                                                                                     //개인키로 활성화
        oDec.FromXmlString(sPrvKey);

        //sValue문자열을 바이트배열로 변환
        byte[] srcbuf = Convert.FromBase64String(sValue);

        //바이트배열 복호화
        byte[] decbuf = oDec.Decrypt(srcbuf, false);

        //복호화 바이트배열을 문자열로 변환
        string sDec = (new UTF8Encoding()).GetString(decbuf, 0, decbuf.Length);
        return sDec;
    }
}
