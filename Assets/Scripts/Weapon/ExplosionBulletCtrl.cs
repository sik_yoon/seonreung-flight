﻿using UnityEngine;
using System.Collections;

public class ExplosionBulletCtrl : BulletCtrl
{
    public ParticleSystem launchParticle;
    public ParticleSystem explosionParticle;
    public AudioSource explosionSound;
    public GameObject bulletModel;

    private int explosionRange;
    private bool isCollide;

    void Awake()
    {
        Init();
    }

    void OnEnable()
    {
        isCollide = false;
        bulletModel.SetActive(true);
        StartCoroutine(EaseInMoveRoutine());
    }

    public void SetStatus(int damage, float speed, int explosionRange)
    {
        this.damage = damage;
        this.speed = speed;
        this.explosionRange = explosionRange;
    }

    protected IEnumerator EaseInMoveRoutine()
    {
        float delta = 0;
        while(!isCollide && tr.position.z < 45 && -5 < tr.position.z)
        {
            tr.Translate(Vector3.forward * delta * Time.deltaTime, Space.Self);
            delta += Time.deltaTime * speed;

            yield return null;
        }

        if(!isCollide) ObjectPoolManager.instance.ReturnPooledObject(gameObject);
    }

    IEnumerator ExplosionRoutine()
    {
        isCollide = true;

        bulletModel.SetActive(false);
        launchParticle.Stop();

        explosionParticle.startSize = explosionRange * 2;
        explosionParticle.Play();
        explosionSound.Play();

        yield return new WaitForSeconds(2.0f);

        ObjectPoolManager.instance.ReturnPooledObject(gameObject);
    }

    protected override void OnTriggerEnter(Collider coll)
    {
        if(isCollide || coll.CompareTag("Bullet") || coll.CompareTag("Item")) return;

        else if(coll.CompareTag("Enemy"))
        {
            Collider[] colls = Physics.OverlapSphere(coll.transform.position, explosionRange, 512);

            for(int i = 0 ; i < colls.Length ; i++)
            {
                colls[i].GetComponent<EnemyCtrl>().Attacked(damage);
            }
        }

        else if(coll.CompareTag("Player")) PlayerCtrl.instance.Attacked();

        StartCoroutine(ExplosionRoutine());
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(tr.position, explosionRange);
    }
}
