﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public abstract class Weapon : MonoBehaviour
{
    protected Transform shootPointTr;
    protected float ypos = 2.0f;

    protected AudioSource shootSound;

    protected virtual void Init()
    {
        shootPointTr = transform.Find("ShootPoint");
        shootSound = GetComponent<AudioSource>();
    }

    protected abstract IEnumerator ShootRoutine(int level);
    public IEnumerator ShootRoutine()
    {
        yield return StartCoroutine(ShootRoutine(GetUpgradeLevel()));

        yield return new WaitForSeconds(GetCurrentReloadTime());
    }
    public IEnumerator PreviewShootRoutine()
    {
        yield return StartCoroutine(ShootRoutine(GetMaxUpgradeLevel()));

        yield return new WaitForSeconds(GetReloadTime(GetMaxUpgradeLevel()));
    }

    public virtual void StopShoot() { }
    public virtual void StartShoot() { }

    public int GetUpgradeLevel()
    {
        return PlayerPrefs.GetInt(name);
    }
    public abstract int GetMaxUpgradeLevel();

    protected abstract int GetUpgradeCost(int level);
    public int GetCurrentUpgradeCost()
    {
        return GetUpgradeCost(GetUpgradeLevel());
    }

    protected abstract int GetDamage(int level);
    public int GetCurrentDamage()
    {
        return GetDamage(GetUpgradeLevel());
    }
    public int GetNextDamage()
    {
        return GetDamage(GetUpgradeLevel() + 1);
    }

    protected abstract float GetReloadTime(int level);
    public float GetCurrentReloadTime()
    {
        return GetReloadTime(GetUpgradeLevel());
    }
    public float GetNextReloadTime()
    {
        return GetReloadTime(GetUpgradeLevel() + 1);
    }

    public void Upgrade()
    {
        PlayerPrefs.SetInt(name, GetUpgradeLevel() + 1);
    }

}
