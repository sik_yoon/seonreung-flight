﻿using UnityEngine;
using System.Collections;
using System;

public class CrossbowWeapon : BulletWeapon
{
    public override int GetMaxUpgradeLevel()
    {
        return 5;
    }

    protected override IEnumerator ShootRoutine(int level)
    {
        shootSound.Play();

        int damage = GetDamage(level);
        float bulletSpeed = GetBulletSpeed(level);

        AllocateBullet("B-Arrow1", 0, damage, bulletSpeed);
        AllocateBullet("B-Arrow1", -5, damage, bulletSpeed);
        AllocateBullet("B-Arrow1", 5, damage, bulletSpeed);
        AllocateBullet("B-Arrow1", -10, damage, bulletSpeed);
        AllocateBullet("B-Arrow1", 10, damage, bulletSpeed);

        yield return null;
    }

    protected override int GetBulletNum(int level)
    {
        return 5;
    }

    protected override float GetBulletSpeed(int level)
    {
        return 30 + level;
    }

    protected override int GetDamage(int level)
    {
        return 1 + level / 2;
    }

    protected override float GetReloadTime(int level)
    {
        return 0.8f - level * 0.1f;
    }

    protected override int GetUpgradeCost(int level)
    {
        return 200 + 100 * level;
    }

    void Awake()
    {
        Init();
    }
}
