﻿using UnityEngine;
using System.Collections;

public abstract class BulletWeapon : Weapon
{
    public GameObject muzzleFlash;

    protected abstract float GetBulletSpeed(int level);
    protected float GetCurrentBulletSpeed()
    {
        return GetBulletSpeed(GetUpgradeLevel());
    }

    protected abstract int GetBulletNum(int level);
    public int GetCurrentBulletNum()
    {
        return GetBulletNum(GetUpgradeLevel());
    }
    public int GetNextBulletNum()
    {
        return GetBulletNum(GetUpgradeLevel() + 1);
    }

    protected void AllocateBullet(string bulletName, int degree, int damage, float speed)
    {
        shootPointTr.Rotate(0, degree, 0);

        BulletCtrl bullet = ObjectPoolManager.instance.GetPooledObject(bulletName, shootPointTr).GetComponent<BulletCtrl>();
        bullet.SetStatus(damage, speed);
        bullet.gameObject.layer = 8;
        bullet.gameObject.SetActive(true);

        shootPointTr.Rotate(0, -degree, 0);
    }

    protected virtual IEnumerator MuzzleFlashRoutine()
    {
        muzzleFlash.transform.Rotate(0, 0, Random.Range(1, 360));
        muzzleFlash.SetActive(true);

        yield return new WaitForSeconds(0.1f);

        muzzleFlash.SetActive(false);
    }
}
