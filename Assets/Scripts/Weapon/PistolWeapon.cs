﻿using UnityEngine;
using System.Collections;

public class PistolWeapon : BulletWeapon
{
    public override int GetMaxUpgradeLevel()
    {
        return 10;
    }

    protected override IEnumerator ShootRoutine(int level)
    {
        shootSound.Play();
        StartCoroutine(MuzzleFlashRoutine());

        int damage = GetDamage(level);
        float bulletSpeed = GetBulletSpeed(level);

        AllocateBullet("B-Normal1", 0, damage, bulletSpeed);

        yield return null;
    }

    protected override int GetBulletNum(int level)
    {
        return 1;
    }

    protected override float GetBulletSpeed(int level)
    {
        return 20;
    }

    protected override int GetDamage(int level)
    {
        return 1 + level / 2;
    }

    protected override float GetReloadTime(int level)
    {
        return 1.05f - level * 0.05f;
    }

    protected override int GetUpgradeCost(int level)
    {
        return 50 + 50 * level;
    }

    void Awake()
    {
        Init();
    }
}
