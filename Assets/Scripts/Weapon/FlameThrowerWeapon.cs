﻿using UnityEngine;
using System.Collections;
using System;

public class FlameThrowerWeapon : Weapon
{
    public ParticleSystem particle;
    public CapsuleCollider weaponColl;

    public override void StartShoot()
    {
        shootSound.Play();
    }

    public override void StopShoot()
    {
        particle.Stop();
        shootSound.Stop();
    }

    public override int GetMaxUpgradeLevel()
    {
        return 3;
    }

    protected override int GetDamage(int level)
    {
        return 1 + level / 2;
    }

    protected override float GetReloadTime(int level)
    {
        return 0.5f;
    }

    protected override int GetUpgradeCost(int level)
    {
        return 200 + 100 * level;
    }

    protected virtual int GetAttackRange(int level)
    {
        return 10 + level * 10;
    }
    public int GetCurrentAttackRange()
    {
        return GetAttackRange(GetUpgradeLevel());
    }
    public int GetNextAttackRange()
    {
        return GetAttackRange(GetUpgradeLevel() + 1);
    }

    protected override IEnumerator ShootRoutine(int level)
    {
        particle.Play();

        float attackRange = GetAttackRange(level);
        weaponColl.height = attackRange;
        weaponColl.center = new Vector3(0, 2, attackRange / 2);

        particle.startLifetime = 0.3f * level;

        weaponColl.enabled = true;

        yield return new WaitForFixedUpdate();

        weaponColl.enabled = false;
    }

    void Awake()
    {
        Init();

        gameObject.layer = 8;
    }

    void OnTriggerEnter(Collider coll)
    {
        int damage = GetCurrentDamage();

        if(coll.CompareTag("Enemy"))
        {
            EnemyCtrl enemy = coll.GetComponent<EnemyCtrl>();
            enemy.Attacked(damage);
        }
    }
}
