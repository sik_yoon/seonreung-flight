﻿using UnityEngine;
using System.Collections;

public class MinigunWeapon : BulletWeapon
{
    private bool isEffectingMuzzleFlash;

    public override int GetMaxUpgradeLevel()
    {
        return 5;
    }

    public override void StartShoot()
    {
        muzzleFlash.SetActive(true);
        shootSound.Play();
    }

    public override void StopShoot()
    {
        muzzleFlash.SetActive(false);
        shootSound.Stop();
    }

    protected override IEnumerator ShootRoutine(int level)
    {
        if(isEffectingMuzzleFlash)
        {
            muzzleFlash.SetActive(false);
            isEffectingMuzzleFlash = false;
        }
        else
        {
            muzzleFlash.transform.Rotate(0, 0, Random.Range(1, 360));
            muzzleFlash.SetActive(true);
            isEffectingMuzzleFlash = true;
        }

        int damage = GetDamage(level);
        float bulletSpeed = GetBulletSpeed(level);

        AllocateBullet("B-Normal1", 0, damage, bulletSpeed);

        yield return null;
    }

    protected override int GetBulletNum(int level)
    {
        return 1;
    }

    protected override float GetBulletSpeed(int level)
    {
        return 30 + level;
    }

    protected override int GetDamage(int level)
    {
        return 1 + level / 2;
    }

    protected override float GetReloadTime(int level)
    {
        return 0.07f;
    }

    protected override int GetUpgradeCost(int level)
    {
        return 200 + 100 * level;
    }

    void Awake()
    {
        Init();
    }
}
