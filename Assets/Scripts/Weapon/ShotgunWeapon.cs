﻿using UnityEngine;
using System.Collections;
using System;

public class ShotgunWeapon : BulletWeapon
{
    public override int GetMaxUpgradeLevel()
    {
        return 5;
    }

    protected override IEnumerator ShootRoutine(int level)
    {
        shootSound.Play();
        StartCoroutine(MuzzleFlashRoutine());

        int damage = GetDamage(level);
        float bulletSpeed = GetBulletSpeed(level);

        AllocateBullet("B-Normal2", 0, damage, bulletSpeed);
        AllocateBullet("B-Normal2", -10, damage, bulletSpeed);
        AllocateBullet("B-Normal2", 10, damage, bulletSpeed);
        if(level >= 3)
        {
            AllocateBullet("B-Normal2", -5, damage, bulletSpeed);
            AllocateBullet("B-Normal2", 5, damage, bulletSpeed);
        }

        yield return null;
    }

    protected override int GetBulletNum(int level)
    {
        if(GetUpgradeLevel() >= 3) return 5;

        else return 3;
    }

    protected override float GetBulletSpeed(int level)
    {
        return 20 + level;
    }

    protected override int GetDamage(int level)
    {
        return 1 + level / 2;
    }

    protected override float GetReloadTime(int level)
    {
        return 2 - level * 0.05f;
    }

    protected override int GetUpgradeCost(int level)
    {
        return 200 + 100 * level;
    }

    void Awake()
    {
        Init();
    }
}
