﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class BulletCtrl : MonoBehaviour
{
    protected Transform tr;

    protected int damage;
    protected float speed;
    public Color color = Color.white;

    protected virtual void Init()
    {
        tr = GetComponent<Transform>();

        Renderer[] renderers = GetComponentsInChildren<Renderer>();
        for(int i = 0 ; i < renderers.Length ; i++)
            renderers[i].material.color = color;

        tag = "Bullet";
    }

    void Awake()
    {
        Init();
    }

    void OnEnable()
    {
        StartCoroutine(LinearMoveRoutine());
    }

    public void SetStatus(int damage, float speed)
    {
        this.damage = damage;
        this.speed = speed;
    }

    protected IEnumerator LinearMoveRoutine()
    {
        while(tr.position.z < 40 && -5 < tr.position.z)
        {
            tr.Translate(Vector3.forward * speed * Time.deltaTime, Space.Self);

            yield return null;
        }

        ObjectPoolManager.instance.ReturnPooledObject(gameObject);
    }

    protected virtual void OnTriggerEnter(Collider coll)
    {
        if(coll.CompareTag("Bullet") || coll.CompareTag("Item")) return;

        else if(coll.CompareTag("Enemy"))
        {
            EnemyCtrl enemy = coll.GetComponent<EnemyCtrl>();
            enemy.Attacked(damage);
        }

        else if(coll.CompareTag("Player")) PlayerCtrl.instance.Attacked();

        // if(info.particleName != "")
        //         ObjectPoolManager.instance.GetPooledObject(info.particleName, tr.position).SetActive(true);

        ObjectPoolManager.instance.ReturnPooledObject(gameObject);
    }
}
