﻿using UnityEngine;
using System.Collections;

public class RPGWeapon : BulletWeapon {
    public override int GetMaxUpgradeLevel()
    {
        return 3;
    }

    protected override int GetBulletNum(int level)
    {
        return 1;
    }

    protected override float GetBulletSpeed(int level)
    {
        return 40;
    }

    protected override int GetDamage(int level)
    {
        return 2;
    }

    protected override float GetReloadTime(int level)
    {
        return 2.5f - 0.5f * level;
    }

    protected override int GetUpgradeCost(int level)
    {
        return 100 + level * 100;
    }

    protected virtual int GetExplosionRange(int level)
    {
        return level * 3;
    }
    public int GetCurrentExplosionRange()
    {
        return GetExplosionRange(GetUpgradeLevel());
    }
    public int GetNextExplosionRange()
    {
        return GetExplosionRange(GetUpgradeLevel() + 1);
    }

    protected void AllocateExplosionBullet(string bulletName, int degree, int damage, float speed, int explosionRange)
    {
        shootPointTr.Rotate(0, degree, 0);

        ExplosionBulletCtrl bullet = ObjectPoolManager.instance.GetPooledObject(bulletName, shootPointTr).GetComponent<ExplosionBulletCtrl>();
        bullet.SetStatus(damage, speed, explosionRange);
        bullet.gameObject.layer = 8;
        bullet.gameObject.SetActive(true);

        shootPointTr.Rotate(0, -degree, 0);
    }

    protected override IEnumerator ShootRoutine(int level)
    {
        int damage = GetDamage(level);
        float bulletSpeed = GetBulletSpeed(level);
        int explosionRange = GetExplosionRange(level);

        AllocateExplosionBullet("B-RPG", 0, damage, bulletSpeed, explosionRange);

        yield return null;
    }

    void Awake()
    {
        Init();
    }
}
