﻿using UnityEngine;
using System.Collections;

public class LightCtrl : MonoBehaviour
{
    public static LightCtrl instance;
    private Light sun;

    void Awake()
    {
        instance = this;

        sun = GetComponent<Light>();
    }

    void Start()
    {
        StartCoroutine(DecreaseLightIntensityRoutine());
    }

    IEnumerator DecreaseLightIntensityRoutine()
    {
        while(true)
        {
            // sun.intensity -= 0.001f * Time.deltaTime;

            // if(sun.intensity == 0) sun.intensity = 1;

            transform.Rotate(0, Time.deltaTime * 7, 0);

            yield return null;
        }
    }
}
