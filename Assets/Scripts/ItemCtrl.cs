﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SphereCollider))]
public class ItemCtrl : MonoBehaviour
{
    private Transform tr;

    void Awake()
    {
        tr = GetComponent<Transform>();

        tag = "Item";
        gameObject.layer = 10;
    }

    void Update()
    {
        tr.Rotate(Vector3.up * Time.timeScale);
    }

    public virtual void Get()
    {
        ObjectPoolManager.instance.ReturnPooledObject(gameObject);
    }

    void OnTriggerEnter(Collider coll)
    {
        if(coll.CompareTag("Bullet"))
        {
            Get();

            ObjectPoolManager.instance.ReturnPooledObject(gameObject);
        }
    }
}
