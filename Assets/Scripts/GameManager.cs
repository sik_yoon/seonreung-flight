﻿using System.Collections;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    private int earnGold;

    private int comboCount;
    public int maxComboCount = 10;

    void Awake()
    {
        instance = this;

        Application.targetFrameRate = 60;

        PlayerPrefs.DeleteAll();

        if(PlayerPrefs.GetInt("C-BusinessMan") == 0)
            PlayerPrefs.SetInt("C-BusinessMan", 1);
    }

    void Start()
    {
        SetGoldAmount(10000);
        SetClearedStage(0);

        GoToLobby();
    }

    public void GameStart()
    {
        UIManager.instance.ChangeUIToInGame();

        UIManager.instance.moveSlider.SetValueToZero();
        UIManager.instance.moveSlider.SetInteractable(true);

        SpawnManager.instance.StartSpawn(GetClearedStage());

        earnGold = 0;

        PlayerCtrl.instance.StartMove();
    }

    public void GameOver()
    {
        UIManager.instance.inGameUI.SetGameOverUIActive(true);

        SetGoldAmount(GetGoldAmount() + earnGold);
    }

    public void GoToLobby()
    {
        ObjectPoolManager.instance.ReturnAllPooledObjects();

        UIManager.instance.SetGoldText(GetGoldAmount());
        UIManager.instance.ChangeUIToLobby();

        PlayerCtrl.instance.ResetStatus();
    }

    public void EarnGold(int amount)
    {
        earnGold += amount;

        UIManager.instance.SetGoldText(earnGold);
    }

    public string GetSelectedCharacterName()
    {
        return PlayerPrefs.GetString("SelectedCharacterName", "C-BusinessMan");
    }

    public void SetSelectedCharacterName(string characterName)
    {
        PlayerPrefs.SetString("SelectedCharacterName", characterName);
        PlayerPrefs.Save();
    }

    public int GetGoldAmount()
    {
        return PlayerPrefs.GetInt("Golds");
    }

    public void SetGoldAmount(int amount)
    {
        PlayerPrefs.SetInt("Golds", amount);
        PlayerPrefs.Save();
    }

    public int GetClearedStage()
    {
        return PlayerPrefs.GetInt("ClearedStage", 0);
    }

    public void SetClearedStage(int stage)
    {
        PlayerPrefs.SetInt("ClearedStage", stage);
        PlayerPrefs.Save();
    }
}
