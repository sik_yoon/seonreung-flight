﻿Shader "Custom/Custom-Outlined" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		_PartsColor ("Parts Color", Color) = (1,1,1,1)
		_OutlineColor ("Outline Color", Color) = (0,0,0,1)
		_Outline ("Outline width", Range (.001, 0.03)) = .002
		_MainTex ("Albedo (RGB)", 2D) = "white" { }
		[Toggle(_INTERPOLATED_PARTS_COLOR)] _InterpolatedPartsColor ("Interpolated Parts Color", Int) = 0
	}

	CGINCLUDE
	#include "UnityCG.cginc"

	struct appdata {
		float4 vertex : POSITION;
		float3 normal : NORMAL;
	};

	struct v2f {
		float4 pos : POSITION;
		float4 color : COLOR;
	};

	uniform float _Outline;
	uniform float4 _OutlineColor;

	v2f vert(appdata v) {
		// just make a copy of incoming vertex data but scaled according to normal direction
		v2f o;
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex);

		float3 norm   = mul ((float3x3)UNITY_MATRIX_IT_MV, v.normal);
		float2 offset = TransformViewToProjection(norm.xy);

		o.pos.xy += offset * o.pos.z * _Outline;
		o.color = _OutlineColor;
		return o;
	}
	ENDCG

	SubShader {
		Tags { "RenderType"="Opaque" }

		CGPROGRAM
		#pragma surface surf Lambert
		#pragma shader_feature _INTERPOLATED_PARTS_COLOR
		#pragma shader_feature _INVERT_PARTS_COLOR_MASK

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		fixed4 _Color;
		fixed4 _PartsColor;

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			c.a = 1 - c.a;

#if _INTERPOLATED_PARTS_COLOR
			o.Albedo = lerp(c.rgb, _PartsColor.rgb, c.a);
#else
			o.Albedo = c.rgb * lerp( float3(1,1,1), _PartsColor.rgb, c.a);
#endif
			o.Alpha = 1;
		}
		ENDCG

		// note that a vertex shader is specified here but its using the one above
		Pass {
			Name "OUTLINE"
			Tags { "LightMode" = "Always" }
			Cull Front
			ZWrite On
			ColorMask RGB
			Blend SrcAlpha OneMinusSrcAlpha
			//Offset 50,50

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			half4 frag(v2f i) :COLOR { return i.color; }
			ENDCG
		}
	}

	Fallback "Diffuse"
}